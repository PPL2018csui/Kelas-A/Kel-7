'use strict';

const line = require('@line/bot-sdk');
const express = require('express');
const handlers = require('./handlers.js');

// create LINE SDK config from env variables
const config = {
  channelAccessToken: process.env.channelAccessToken,
  channelSecret: process.env.channelSecret
};

// create LINE SDK client
const client = new line.Client(config);
exports.client = client;
// create Express app
// about Express itself: https://expressjs.com/
const app = express();

// register a webhook handler with middleware
// about the middleware, please refer to doc
app.post('/callback', line.middleware(config), (req, res) => {
  Promise
    .all(req.body.events.map(handlers.handleEvent))
    .then((result) => res.json(result))
    .catch((err) => {
      console.error(err);
      res.status(500).end();
    });
});

// listen on port
const port = process.env.PORT || 3000;
console.log("jalan");
app.listen(port);