/* jshint expr: true */
'use strict';

var expect = require("chai").expect;
var sinon = require("sinon");
var line = require("../index.js");
var handlers = require("../handlers.js");
var client = line.client;
var func = require("../utils/utils_bot.js");

describe('testing command show', () =>{
    var stubDB;
    var stub;
    var stubProfileGroup;
    var stubProfileRoom;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
        stubDB = sinon.stub(func.database, "query");
        stubProfileGroup = sinon.stub(client, "getGroupMemberProfile");
        stubProfileRoom = sinon.stub(client, "getRoomMemberProfile");
    });

    afterEach(function(){
        stub.restore();
        stubDB.restore();
        stubProfileGroup.restore();
        stubProfileRoom.restore();
    });

    it("should return a text message with a status 1 message GROUP", ()=>{
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: [{event_name:'kicut', id_event_types: 0}]});
        stubDB.onCall(1).resolves({rows: [{event_name:'kicut', nama_partisipan: 'wew', user_id: 'kakjshdouahs', user_comment: 'weleh'}]});
        stubProfileGroup.resolves({displayName: 'Nama'});
        const expected = [
            {
                type: 'text',
                status: '1',
                text: "Yang berhasil join 'kicut'"
            },
            {
                type: "template",
                altText: "this is a confirm template",
                template: {
                        type: "buttons",
                        text: 'result.template.message.text',
                        actions: [
                            'result.template.action'
                        ]
                }
            }
        ];
        //console.log('wow');
        return handlers.handleEvent(event).then(result=>{
            return expect(result[0].status).equals(expected[0].status);
        });
    });

    it("should return a text message with a status 1 message GROUP", ()=>{
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: [{event_name:'kicut', id_event_types: 1}]});
        stubDB.onCall(1).resolves({rows: [{event_name:'kicut', nama_partisipan: 'wew', user_id: 'kakjshdouahs', user_comment: 'weleh'}]});
        stubProfileGroup.resolves({displayName: 'Nama'});
        const expected = [
            {
                type: 'text',
                status: '1',
                text: "Yang berhasil join 'kicut'"
            },
            {
                type: "template",
                altText: "this is a confirm template",
                template: {
                        type: "buttons",
                        text: 'result.template.message.text',
                        actions: [
                            'result.template.action'
                        ]
                }
            }
        ];
        //console.log('wow');
        return handlers.handleEvent(event).then(result=>{
            return expect(result[0].status).equals(expected[0].status);
        });
    });

    it("should return a text message with a status 1 message ROOM", ()=>{
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: [{event_name:'kicut'}]});
        stubDB.onCall(1).resolves({rows: [{event_name:'kicut', nama_partisipan: 'wew', user_id: 'kakjshdouahs', user_comment: 'weleh'}]});
        stubProfileRoom.resolves({displayName: 'Nama'});
        const expected = [
            {
                type: 'text',
                status: '1',
                text: "Yang berhasil join 'kicut'"
            },
            {
                type: "template",
                altText: "this is a confirm template",
                template: {
                        type: "buttons",
                        text: 'result.template.message.text',
                        actions: [
                            'result.template.action'
                        ]
                }
            }
        ];
        //console.log('wow');
        return handlers.handleEvent(event).then(result=>{
            return expect(result[0].status).equals(expected[0].status);
        });
    });

    it("should return a button template GROUP", ()=>{
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: [{event_name:'kicut', id_event_types: 0}]});
        stubDB.onCall(1).resolves({rows: [{event_name:'kicut', nama_partisipan: 'wew', user_id: 'kakjshd', user_comment: null}]});
        stubProfileGroup.resolves({displayName: 'Nama'});
        const expected = [
            {
                type: 'text',
                status: '1',
                text: 'Yang berhasil join wawawea lezatos'
            },
            {
                type: "template",
                altText: "this is a confirm template",
                template: {
                        type: "buttons",
                        text: 'result.template.message.text',
                        actions: [
                            'result.template.action'
                        ]
                }
            }
        ];
        return handlers.handleEvent(event).then(result=>{
            return expect(result[1].template.type).equals(expected[1].template.type);
        });
    });

    it("should return a button template ROOM", ()=>{
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: [{event_name:'kicut', id_event_types: 0}]});
        stubDB.onCall(1).resolves({rows: [{event_name:'kicut', nama_partisipan: 'wew', user_id: 'kakjshd', user_comment: null}]});
        stubProfileRoom.resolves({displayName: 'Nama'});
        const expected = [
            {
                type: 'text',
                status: '1',
                text: 'Yang berhasil join wawawea lezatos'
            },
            {
                type: "template",
                altText: "this is a confirm template",
                template: {
                        type: "buttons",
                        text: 'result.template.message.text',
                        actions: [
                            'result.template.action'
                        ]
                }
            }
        ];
        return handlers.handleEvent(event).then(result=>{
            return expect(result[1].template.type).equals(expected[1].template.type);
        });
    });

    it("should reject failed db request GROUP", ()=>{
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).rejects();
        stubProfileGroup.resolves({displayName: 'Nama'});
        const expected =
            {
                type: 'text',
                text: 'Koneksi bermasalah'
            };
        return handlers.handleEvent(event).then(result=>{
            return expect(result).include(expected);
        });
    });

    it("should reject failed db request ROOM", ()=>{
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).rejects();
        stubProfileRoom.resolves({displayName: 'Nama'});
        const expected =
            {
                type: 'text',
                text: 'Koneksi bermasalah'
            };
        return handlers.handleEvent(event).then(result=>{
            return expect(result).include(expected);
        });
    });

    it('should return a message notifying that no event has been created GROUP', () => {
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: []});
        stubDB.onCall(1).resolves({rows: []});
        stubDB.onCall(2).resolves({rows: []});
        stubProfileGroup.resolves({displayName: 'Nama'});
        var button = {
            type: 'template',
            altText: 'Ketik /create untuk membuat event',
            template: {
                type: 'buttons',
                text: 'Buat event?',
                actions: [
                    {
                        type: 'message',
                        label: 'Ya',
                        text: '/create'
                    }
                ]
            }
        };

        var message = {
            type: 'text',
            text: 'Belum ada event atuh bro.....\nketik /create_event untuk buat event baru'
        };

        const expected = [message, button];

        return handlers.handleEvent(event).then(result=>{
            return expect(result[0]).include(expected[0]);
        });
    });

    it('should return a message notifying that no event has been created ROOM', () => {
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: []});
        stubDB.onCall(1).resolves({rows: []});
        stubDB.onCall(2).resolves({rows: []});
        stubProfileRoom.resolves({displayName: 'Nama'});
        var button = {
            type: 'template',
            altText: 'Ketik /create untuk membuat event',
            template: {
                type: 'buttons',
                text: 'Buat event?',
                actions: [
                    {
                        type: 'message',
                        label: 'Ya',
                        text: '/create'
                    }
                ]
            }
        };

        var message = {
            type: 'text',
            text: 'Belum ada event atuh bro.....\nketik /create_event untuk buat event baru'
        };

        const expected = [message, button];

        return handlers.handleEvent(event).then(result=>{
            return expect(result[0]).include(expected[0]);
        });
    });

    it('should return a button prompting user to create an event GROUP', () => {
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: []});
        stubDB.onCall(1).resolves({rows: []});
        stubDB.onCall(2).resolves({rows: []});
        stubProfileGroup.resolves({displayName: 'Nama'});
        var button = {
            type: 'template',
            altText: 'Ketik /create untuk membuat event',
            template: {
                type: 'buttons',
                text: 'Buat event?',
                actions: [
                    {
                        type: 'message',
                        label: 'Ya',
                        text: '/create'
                    }
                ]
            }
        };

        var message = {
            type: 'text',
            text: 'Belum ada event atuh bro.....\nketik /create_event untuk buat event baru'
        };

        const expected = [message, button];

        return handlers.handleEvent(event).then(result=>{
            return expect(result[1].type).equals(expected[1].type);
        });
    });

    it('should return a button prompting user to create an event ROOM', () => {
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: []});
        stubDB.onCall(1).resolves({rows: []});
        stubDB.onCall(2).resolves({rows: []});
        stubProfileRoom.resolves({displayName: 'Nama'});
        var button = {
            type: 'template',
            altText: 'Ketik /create untuk membuat event',
            template: {
                type: 'buttons',
                text: 'Buat event?',
                actions: [
                    {
                        type: 'message',
                        label: 'Ya',
                        text: '/create'
                    }
                ]
            }
        };

        var message = {
            type: 'text',
            text: 'Belum ada event atuh bro.....\nketik /create_event untuk buat event baru'
        };

        const expected = [message, button];

        return handlers.handleEvent(event).then(result=>{
            return expect(result[1].type).equals(expected[1].type);
        });
    });

    it('should return a list with multiple option', () => {
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: [{group_id: 'adjaowdjkwad', event_name:'kicut kicut rumah', id_event_types: 2, is_active: true}]});
        stubDB.onCall(1).resolves({rows: [{group_id: 'adjaowdjkwad', nama_partisipan: 'kicut', id_option: 1, user_id: 'kakjshdouahs'},{group_id: 'adjaowdjkwad', nama_partisipan: 'kicut', id_option: 2, user_id: 'wiuuuw'}]});
        stubDB.onCall(2).resolves({rows: [{group_id: 'adjaowdjkwad', id_option: 1, option_name: 'Bryanza'}, {group_id: 'adjaowdjkwad', id_option: 2, option_name: 'Novirahman'}]});
        stubProfileRoom.resolves({displayName: 'Nama'});
        var button = {
            type: 'template',
            altText: 'Ketik /create untuk membuat event',
            template: {
                type: 'buttons',
                text: 'Buat event?',
                actions: [
                    {
                        type: 'message',
                        label: 'Ya',
                        text: '/create'
                    }
                ]
            }
        };

        return handlers.handleEvent(event).then(result => {
            return expect(result[0].text).equals("kicut kicut rumah:\n*Bryanza:\n   1. kicut\n*Novirahman:\n   1. kicut\n");
        });

    });

    it('should return a list with multiple option, Option name exceed 75', () => {
        const msg = {
            type : 'text',
            text : '/show'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows: [{group_id: 'adjaowdjkwad', event_name:'kicut kicut rumah', id_event_types: 2, is_active: true}]});
        stubDB.onCall(1).resolves({rows: [{group_id: 'adjaowdjkwad', nama_partisipan: 'kicut', id_option: 1, user_id: 'kakjshdouahs'},{group_id: 'adjaowdjkwad', nama_partisipan: 'kicut', id_option: 2, user_id: 'wiuuuw'}]});
        stubDB.onCall(2).resolves({rows: [{group_id: 'adjaowdjkwad', id_option: 1, option_name: 'Bryanzaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'}, {group_id: 'adjaowdjkwad', id_option: 2, option_name: 'Novirahman'}]});
        stubProfileRoom.resolves({displayName: 'Nama'});
        var button = {
            type: 'template',
            altText: 'Ketik /create untuk membuat event',
            template: {
                type: 'buttons',
                text: 'Buat event?',
                actions: [
                    {
                        type: 'message',
                        label: 'Ya',
                        text: '/create'
                    }
                ]
            }
        };

        return handlers.handleEvent(event).then(result => {
            return expect(result[0].text).equals("kicut kicut rumah:\n*Bryanzaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa:\n   1. kicut\n*Novirahman:\n   1. kicut\n");
        });

    });


});
