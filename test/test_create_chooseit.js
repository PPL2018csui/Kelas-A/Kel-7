/*jshint expr: true*/
'use strict';

var expect = require("chai").expect;
var chai = require("chai");
var sinon = require("sinon");
var line = require("../index.js");
var handlers = require("../handlers.js");
var client = line.client;
var func = require("../utils/utils_bot.js");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
chai.should();

describe ("test when creating event with multiple choices (/create chooseit_)", () =>{
	var stub;
    var stubDB;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
        stubDB = sinon.stub(func.database, "query");

    });

    afterEach(function(){
        stubDB.restore();
        stub.restore();
    });

    it("should return 'maksimal opsi adalah 4' message", () => {
    	const msg = {
            type : 'text',
            text : '/create chooseit_7'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);

        var res = handlers.handleEvent(event);
        expect(res.text).equal('Jumlah maksimal opsi hanya 4');

    });

    it("should successfully created event with multiple choices", () => {
    	const msg = {
            type : 'text',
            text : '/create chooseit_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.resolves({status: "ok"});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Masukkan opsi sebanyak yang anda pilih dengan perintah /opsi ...\ncontoh "/opsi1 hari minggu"');
        });
    });

    it("should fail to create choices of an event due to the amount of choices already defined/chosen", () => {
    	const msg = {
            type : 'text',
            text : '/create chooseit_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).rejects({code : '23505'});
        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Jumlah opsi pada event ini telah dipilih!');
        });
    });

    it("should fail to create choices of an event due to server error", () => {
    	const msg = {
            type : 'text',
            text : '/create chooseit_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).rejects({status : 'fail'});
        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });
    });
});

describe("test command '/opsi confirm'", () => {
	var stub;
    var stubDB;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
        stubDB = sinon.stub(func.database, "query");

    });

    afterEach(function(){
        stubDB.restore();
        stub.restore();
    });

    it("should return successfully confirm given options message", () => {
        const msg = {
            type : 'text',
            text : '/opsi confirm'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows:[{count:"1"}]});
		stubDB.onCall(1).resolves({rows:[{id_option: '1', option_name: 'test'}]});

        return handlers.handleEvent(event).then(result => {
            expect(result[1].type).equal('template');
        });

    });

    it("should return failed to confirm given options due to server error", () => {
        const msg = {
            type : 'text',
            text : '/opsi confirm'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).rejects({status : 'fail'});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });

    });

});

describe("test command '/opsi'", () => {
    var stub;
    var stubDB;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
        stubDB = sinon.stub(func.database, "query");

    });

    afterEach(function(){
        stubDB.restore();
        stub.restore();
    });

    it("should successfully named all options", () => {
        const msg = {
            type : 'text',
            text : '/opsi3 hari kamis'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubDB.onCall(1).resolves({rows:[{count:"1"}]});
        stubDB.onCall(2).resolves({rows:[]});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Event telah terhapus');
        });

    });

    it("should successfully named an option", () => {
        const msg = {
            type : 'text',
            text : '/opsi3 hari kamis'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubDB.onCall(1).resolves({rows:[{count:"1"}]});
        stubDB.onCall(2).resolves({"rows":[
                  {
                     "id_option":2,
                     "option_name":null
                  },
                  {
                     "id_option":3,
                     "option_name":"null"
                  },
                  {
                     "id_option":1,
                     "option_name":"aljsd"
                  }
               ]
            });

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('List opsi:\nOpsi 2 =  - \nOpsi 3 = null\nOpsi 1 = aljsd\n');
        });

    });

    it("should successfully named an option", () => {
        const msg = {
            type : 'text',
            text : '/opsi3 hari kamis'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubDB.onCall(1).resolves({rows:[{count:"1"}]});
        stubDB.onCall(2).resolves(
            {"rows":[
                  {
                     "id_option":2,
                     "option_name":"lajsd"
                  },
                  {
                     "id_option":3,
                     "option_name":"null"
                  },
                  {
                     "id_option":1,
                     "option_name":"aljsd"
                  }
               ]
            }
            );

        return handlers.handleEvent(event).then(result => {
            expect(result[1].template.title).equal('Konfirmasi Opsi?');
        });

    });

    it("should failed named option due to server error", () => {
        const msg = {
            type : 'text',
            text : '/opsi3 hari kamis'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubDB.onCall(1).resolves({rows:[{count:"1"}]});
        stubDB.onCall(2).rejects({status : 'fail'});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });

    });

    it("should failed named option with message 'Masukkan nomor opsi sesuai dengan pilihan jumlah opsi'", () => {
        const msg = {
            type : 'text',
            text : '/opsi4 hari kamis'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubDB.onCall(1).resolves({rowCount : 0});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Masukkan nomor opsi sesuai dengan pilihan jumlah opsi!');
        });

    });

    it("should failed named option due to server error", () => {
        const msg = {
            type : 'text',
            text : '/opsi3 hari kamis'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubDB.onCall(1).rejects({status : 'fail'});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });

    });

    it("should failed named option and return message 'Opsi telah dikonfirmasi'", () => {
        const msg = {
            type : 'text',
            text : '/opsi3 hari kamis'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rows:[{count:"1"}]});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Opsi telah dikonfirmasi, tidak bisa diubah lagi');
        });

    });

    it("should failed named option due to server error", () => {
        const msg = {
            type : 'text',
            text : '/opsi3 hari kamis'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).rejects({status : 'fail'});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });

    });
});
