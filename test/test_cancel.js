/* jshint expr: true */
'use strict';

var expect = require("chai").expect;
var chai = require("chai");
var sinon = require("sinon");
var line = require("../index.js");
var handlers = require("../handlers.js");
var client = line.client;
var func = require("../utils/utils_bot.js");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
chai.should();

describe('testing cancel feature', () => {
    var stub;
    var stubDB;
    var stubProfileGroup;
    var stubProfileRoom;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
        stubDB = sinon.stub(func.database, "query");
        stubProfileGroup = sinon.stub(client, "getGroupMemberProfile");
        stubProfileRoom = sinon.stub(client, "getRoomMemberProfile");
    });

    afterEach(function(){
        stubProfileGroup.restore();
        stub.restore();
        stubDB.restore();
        stubProfileRoom.restore();
    });

    it ('should return success to cancel event GROUP', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 1
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(2).resolves({rowCount: '1'});
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' membatalkan partisipasi'

        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return success to cancel event ROOM', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
            stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 1
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(2).resolves({rowCount: '1'});
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' membatalkan partisipasi'

        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel event due to server error after event and paticipant check GROUP', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 1
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":0
                         }
                      ]
                   }
                ]
            );
        stubDB.rejects();
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel event due to server error after event and paticipant check ROOM', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 1
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":0
                         }
                      ]
                   }
                ]
            );
        stubDB.rejects();
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel event due to server error after event check GROUP', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.rejects();
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel event due to server error after event check ROOM', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.rejects();
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel due to the server cant get userId GROUP', () => {
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.rejects('gagal');
        stubProfileGroup.rejects();
        const expected = {
            type: 'text',
            text: 'Tambahkan agus sebagai teman sebelum cancel'

        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel due to the server cant get userId ROOM', () => {
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.rejects('gagal');
        stubProfileRoom.rejects();
        const expected = {
            type: 'text',
            text: 'Tambahkan agus sebagai teman sebelum cancel'

        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });


    it ('should return fail to cancel event due to the absence of event GROUP', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.resolves({rowCount: '0'});
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', anda tidak bisa cancel karena event belum dibuat'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel event due to the absence of event ROOM', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.resolves({rowCount: '0'});
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', anda tidak bisa cancel karena event belum dibuat'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel list it event due to not join any event GROUP', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 0
                   },
                   {
                      "command":"SELECT",
                      "rowCount": 1,
                      "rows":[
                         {
                            "id_event_types":0
                         }
                      ]
                   }
                ]
            );
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', anda tidak bisa cancel karena belum join'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel list it event due to not join any event ROOM', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 0
                   },
                   {
                      "command":"SELECT",
                      "rowCount": 1,
                      "rows":[
                         {
                            "id_event_types":0
                         }
                      ]
                   }
                ]
            );
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', anda tidak bisa cancel karena belum join'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });


    it ('should return fail to cancel choose it event due to not join any event GROUP', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 0
                   },
                   {
                      "command":"SELECT",
                      "rowCount": 1,
                      "rows":[
                         {
                            "id_event_types":2
                         }
                      ]
                   }
                ]
            );
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', anda tidak bisa cancel karena belum memilih opsi apapun'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });


    it ('should return fail to cancel choose it event due to not join any event ROOM', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 0
                   },
                   {
                      "command":"SELECT",
                      "rowCount": 1,
                      "rows":[
                         {
                            "id_event_types":2
                         }
                      ]
                   }
                ]
            );
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', anda tidak bisa cancel karena belum memilih opsi apapun'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });


    it ('should return fail to cancel, because there is no event available', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).resolves(
                [
                   {
                      "command":"SELECT",
                      "rowCount": 0
                   },
                   {
                      "command":"SELECT",
                      "rowCount": 0,
                   }
                ]
            );
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', gagal cancel karena event tidak ada'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });


    it ('should return fail to cancel event due to server error GROUP', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.rejects();
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ('should return fail to cancel event due to server error ROOM', () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/cancel'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.rejects();
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ', Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

});
