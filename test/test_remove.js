/* jshint expr: true */
'use strict';

var expect = require("chai").expect;
var chai = require("chai");
var sinon = require("sinon");
var line = require("../index.js");
var handlers = require("../handlers.js");
var client = line.client;
var func = require("../utils/utils_bot.js");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
chai.should();

describe('testing command remove', () =>{
    var stubDB;
    var stub;
    var stubProfile;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
        stubDB = sinon.stub(func.database, "query");
        stubProfile = sinon.stub(client, "getProfile");
    });

    afterEach(function(){
        stub.restore();
        stubDB.restore();
        stubProfile.restore();
    });

    

    it("test remove if there is a legitimate event existed", () =>{

        const msg = {
            type : 'text',
            text : '/remove'
        };

        const event = {
            type : 'message',
            replyToken : '123123',
            source : {groupId : 'thisIsGroupId'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.resolves({rowCount: '1'});

        return handlers.handleEvent(event).then(result => {
            expect(result.type).equals('template');
        });

    });

    it("test remove if there is no legitimate event existed", () =>{

        const msg = {
            type : 'text',
            text : '/remove'
        };

        const event = {
            type : 'message',
            replyToken : '123123',
            source : {groupId : 'thisIsGroupId'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.resolves({rowCount: '0'});
        
        const expected =  {
          	type: 'text',
            text: 'Tidak ada event yang dibuat'
        }; 

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });

    });

    it("test remove if there is no legitimate event existed after confirmation", () =>{

        const msg = {
            type : 'text',
            text : '/remove confirm'
        };

        const event = {
            type : 'message',
            replyToken : '123123',
            source : {groupId : 'thisIsGroupId', userId : 'thisIsUserId'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '0'});
        
        const expected =  {
          	type: 'text',
            text: 'Tidak ada event yang dibuat'
        }; 

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test remove if there is a legitimate event existed after confirmation and done by an admin leading to a successful removal", () =>{

        const msg = {
            type : 'text',
            text : '/remove confirm'
        };

        const event = {
            type : 'message',
            replyToken : '123123',
            source : {groupId : 'thisIsGroupId', userId : 'thisIsUserId'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.resolves({rowCount:1});

        const expected =  {
          	type: 'text', 
          	text: 'Event berhasil dihapus.'
        }; 

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test remove if there is problem while verifying event", () =>{

        const msg = {
            type : 'text',
            text : '/remove confirm'
        };

        const event = {
            type : 'message',
            replyToken : '123123',
            source : {groupId : 'thisIsGroupId', userId : 'thisIsUserId'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.rejects();
        
        const expected =  {
          	type: 'text',
            text: 'Penghapusan gagal pada server. Silahkan tunggu kemudian coba lagi.'
        }; 
        
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test remove if successfully verify the admin but have a problem to delete data in the Database", () =>{

        const msg = {
            type : 'text',
            text : '/remove confirm'
        };

        const event = {
            type : 'message',
            replyToken : '123123',
            source : {groupId : 'thisIsGroupId', userId : 'thisIsUserId'},
            message : msg
        };

        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount: '1'});
        stubDB.onCall(1).rejects();
        
        const expected =  {
            type: 'text',
            text: 'Penghapusan gagal pada server. Silahkan tunggu kemudian coba lagi.'
        }; 
        
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

});
