/*jshint expr: true*/
'use strict';

var expect = require("chai").expect;
var chai = require("chai");
var sinon = require("sinon");
var line = require("../index.js");
var handlers = require("../handlers.js");
var client = line.client;
var func = require("../utils/utils_bot.js");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
chai.should();


describe ("testing for successfully creating event", () => {
    var stub;
    var stubDB;
    before(function(){
        stub = sinon.stub(client, "replyMessage");
        stubDB = sinon.stub(func.database, "query");
        stub.returnsArg(1);
        stubDB.resolves('berhasil');

    });

    after(function(){
        stubDB.restore();
        stub.restore();
    });

    it("test '/create_event', should return a template button to choose what type of event", () => {
        const msg = {
            type : 'text',
            text : '/create_event'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stubDB.resolves({status: "ok"});
        stub.returnsArg(1);

        return handlers.handleEvent(event).then(result => {
            expect(result.template).property("type", "carousel");
        });
    });

    it("test '/create listit', should return a template button to choose what type of List It event", () => {
        const msg = {
            type : 'text',
            text : '/create listit'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        return handlers.handleEvent(event).then(result => {
            expect(result.template).property("type", "carousel");
        });
    });

});



describe ("testing for failed createing event", () => {
    var stub;
    var stubDB;
    before(function(){
        stub = sinon.stub(client, "replyMessage");
        stubDB = sinon.stub(func.database, "query");
        stub.returnsArg(1);
        stubDB.resolves('berhasil');

    });

    after(function(){
        stubDB.restore();
        stub.restore();
    });

    it("test '/create', should return message jenis event belum ditulis", () => {
        const msg = {
            type : 'text',
            text : '/create'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);

        var result = handlers.handleEvent(event);
        expect(result.text).equal('Jenis event belum ditulis');

    });

    it("test '/create_event' while failed for access database, should return a failed message", () => {
        const msg = {
            type : 'text',
            text : '/create_event'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.rejects({status : 'fail'});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });
    });

    // it("test '/create_event' while failed for access database, should return a failed message", () => {
    //     const msg = {
    //         type : 'text',
    //         text : '/create_event'
    //     };
    //     const event = {
    //         type : 'message',
    //         replyToken : '123123',
    //         source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
    //         message : msg
    //     };
    //     stub.returnsArg(1);
    //     stubDB.rejects({code : '23505'});

    //     return handlers.handleEvent(event).then(result => {
    //         expect(result.text).equal('Anda hanya dapat membuat satu event pada sebuah group/multichat.\n' + 'Ketik command "/remove" untuk menghapus event yang sebelumnya anda buat');
    //     });
    // });

    it("test '/create_event' while failed because someone create event twice in the same group, should return message suggestion to remove event", () => {
        const msg = {
            type : 'text',
            text : '/create_event'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.rejects({code:'23505'});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Anda hanya dapat membuat satu event pada sebuah group/multichat.\n' + 'Ketik command "/remove" untuk menghapus event yang sebelumnya anda buat');
        });
    });

    it("test '/create abcd, should return jenis event tidak dikenal", () => {
        const msg = {
            type : 'text',
            text : '/create abcdef'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        var result = handlers.handleEvent(event);

        expect(result.text).equal('Command tidak dikenal');

    });

    it("test '/create abcd, should return jenis event tidak dikenal", () => {
        const msg = {
            type : 'text',
            text : '/createabcdef awfawf awf'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        var result = handlers.handleEvent(event);

        expect(result.text).equal('Command tidak dikenal');

    });

});


describe ("testing for successfully creating event ListIt tanpa argumen", () => {
    var stub;
    var stubDB;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stubDB = sinon.stub(func.database, "query");
        stub.returnsArg(1);
    });

    afterEach(function(){
        stubDB.restore();
        stub.restore();
    });


    it("test '/create listit_1', should return a instruction for setup the event's name", () => {
        const msg = {
            type : 'text',
            text : '/create listit_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );

        stubDB.onCall(1).resolves();
        return handlers.handleEvent(event).then(result => {
            expect(result[1].text).equal('Tentukan nama event anda dengan cara \n' +
                                        'Ketik command /event_name <spasi> Nama Event Anda\n\n' +
                                        'Contoh:\n' +
                                        '/event_name Besok Belajar Bareng Yuk');
        });
    });

    it("test '/event_name', should return a message the event successfully created", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar bareng'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(1).resolves();
        stubDB.onCall(2).resolves(
                [
                   {
                      "command":"UPDATE"
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":0
                         }
                      ]
                   }
                ]
            );

        return handlers.handleEvent(event).then(result => {
            console.log(result);
            expect(result[2].template.title).equal('belajar bareng');
        });
    });

    it("test '/event_name', should return a message the event successfully created", () => {
        const msg = {
            type : 'text',
            text : '/event_name qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcv'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(1).resolves();
        stubDB.onCall(2).resolves(
                [
                   {
                      "command":"UPDATE"
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":0
                         }
                      ]
                   }
                ]
            );

        
        return handlers.handleEvent(event).then(result => {
            expect(result[2].template.title).equal('qwertyuiopasdfghjklzxcvbnmqwertyuiopa...');
        });
    });

});




describe ("testing for successfully creating event ListIt dengan argumen", () => {
    var stub;
    var stubDB;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stubDB = sinon.stub(func.database, "query");
        stub.returnsArg(1);
    });

    afterEach(function(){
        stubDB.restore();
        stub.restore();
    });


    it("test '/create listit_2', should return a instruction for setup the event's name", () => {
        const msg = {
            type : 'text',
            text : '/create listit_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );

        stubDB.onCall(1).resolves();
        return handlers.handleEvent(event).then(result => {
            expect(result[1].text).equal('Tentukan nama event anda dengan cara \n' +
                                        'Ketik command /event_name <spasi> Nama Event Anda\n\n' +
                                        'Contoh:\n' +
                                        '/event_name Besok Belajar Bareng Yuk');
        });
    });

    it("test '/event_name', should return a message the event successfully created", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar bareng'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(1).resolves();
        stubDB.onCall(2).resolves(
                [
                   {
                      "command":"UPDATE"
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":1
                         }
                      ]
                   }
                ]
            );

        var expectedResult = 'Wacana "belajar bareng" berhasil dibuat ^_^\n' +
                                'Ketik command /join <spasi> argumen anda\n\n' +
                                'Contoh:\n' +
                                '/join ini argumen saya';

        return handlers.handleEvent(event).then(result => {
            expect(result[1].text).equal(expectedResult);
        });
    });

});



describe ("testing for failed creating event ListIt", () => {

    var stub;
    var stubDB;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stubDB = sinon.stub(func.database, "query");
        stub.returnsArg(1);
    });

    afterEach(function(){
        stubDB.restore();
        stub.restore();
    });

    it("test '/event_name' while failed for access database when check active event, should return a failed message", () => {
        const msg = {
            type : 'text',
            text : '/create listit'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).rejects();
        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });
    });

    it("test '/create listit', failed because there's no event in the gorup, should return no available event message", () => {
        const msg = {
            type : 'text',
            text : '/create listit'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        return handlers.handleEvent(event).then(result => {
            var expectedResult = 'Maaf, Tidak terdapat event pada group ini\n'+
                                'Silahkan lakukan perintah "/create_event" untuk membuat event baru';
            expect(result.text).equal(expectedResult);
        });
    });

    it("test '/create listit', failed because there's no event in the gorup, should return no available event message", () => {
        const msg = {
            type : 'text',
            text : '/create listit'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   }
                ]
            );
        return handlers.handleEvent(event).then(result => {
            var expectedResult = 'Anda tidak dapat meng-edit event yang telah aktif';
            expect(result.text).equal(expectedResult);
        });
    });

    it("test '/event_name' failed because the event name did not specified", () => {
        const msg = {
            type : 'text',
            text : '/event_name'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        var result = handlers.handleEvent(event);
        stub.returnsArg(1);
        expect(result.text).equal('Nama event belum ditulis\n' +
                                    'Contoh: /event_name belajar besok');
    });

    it("test '/event_name' while failed for access database when check active event, should return a failed message", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar bareng'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).rejects();
        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });
    });

    it("test '/event_name' while failed for access database when check active event, should return a failed message", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar bareng'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(1).resolves();
        stubDB.onCall(2).rejects();
        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });
    });

    it("test '/event_name' while failed for access database when update name, should return a failed message", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar bareng'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(1).rejects({status: 'fail'});
        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });
    });

    it("test '/create listit_1', failed because there's no event in the gorup, should return no available event message", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar bareng'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        return handlers.handleEvent(event).then(result => {
            var expectedResult = 'Maaf, Tidak terdapat event pada group ini\n'+
                                'Silahkan lakukan perintah "/create_event" untuk membuat event baru';
            expect(result.text).equal(expectedResult);
        });
    });

    it("test '/create listit', failed because there's no event in the gorup, should return no available event message", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar bareng'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   }
                ]
            );
        return handlers.handleEvent(event).then(result => {
            var expectedResult = 'Anda tidak dapat meng-edit event yang telah aktif';
            expect(result.text).equal(expectedResult);
        });
    });

});



describe ("testing for failed creating event ListIt Tanpa Argumen", () => {

    var stub;
    var stubDB;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stubDB = sinon.stub(func.database, "query");
        stub.returnsArg(1);
    });

    afterEach(function(){
        stubDB.restore();
        stub.restore();
    });

    it("test '/create listit_1', while failed for access database, should return a failed message", () => {
        const msg = {
            type : 'text',
            text : '/create listit_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.rejects();
        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });
    });

    it("test '/create listit_1', failed because there's no event in the gorup, should return no available event message", () => {
        const msg = {
            type : 'text',
            text : '/create listit_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        return handlers.handleEvent(event).then(result => {
            var expectedResult = 'Maaf, Tidak terdapat event pada group ini\n'+
                                'Silahkan lakukan perintah "/create_event" untuk membuat event baru';
            expect(result.text).equal(expectedResult);
        });
    });


    it("test '/create listit', failed because there's no event in the gorup, should return no available event message", () => {
        const msg = {
            type : 'text',
            text : '/create listit_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   }
                ]
            );
        return handlers.handleEvent(event).then(result => {
            var expectedResult = 'Anda tidak dapat meng-edit event yang telah aktif';
            expect(result.text).equal(expectedResult);
        });
    });

    it("test '/create listit_1', while failed for access database, should return a failed message", () => {
        const msg = {
            type : 'text',
            text : '/create listit_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );

        stubDB.onCall(1).rejects();
        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });
    });

});


describe ("testing for creating event ChooseIt", () => {

    var stub;
    var stubDB;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stubDB = sinon.stub(func.database, "query");
        stub.returnsArg(1);
        stubDB.rejects('gagal');
    });

    afterEach(function(){
        stubDB.restore();
        stub.restore();
    });


    it("test '/create chooseit', should return expected message", () => {
        const msg = {
            type : 'text',
            text : '/create chooseit'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.resolves({status: "ok"});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Tentukan nama event anda dengan cara \n' + 
                                    'Ketik command /event_name <spasi> Nama Event Anda\n\n' +
                                    'Contoh:\n' +
                                    '/event_name Besok Belajar Bareng Yuk');
        });

    });

    it("test '/create chooseit', should fail due to server error", () => {
        const msg = {
            type : 'text',
            text : '/create chooseit'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.rejects({status: "fail"});

        return handlers.handleEvent(event).then(result => {
            expect(result.text).equal('Terjadi kesalahan pada server, silahkan coba beberapa saat lagi');
        });

    });

    it("test '/event_name', should return a message the event successfully created", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar bareng'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(1).resolves();
        stubDB.onCall(2).resolves(
                [
                   {
                      "command":"UPDATE"
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":2
                         }
                      ]
                   }
                ]
            );

        return handlers.handleEvent(event).then(result => {
            expect(result[1].template.title).equal('belajar bareng');
        });
    });



    it("test '/event_name', should return a message the event successfully created", () => {
        const msg = {
            type : 'text',
            text : '/event_name belajar barengalkdjslakjsdlkajsdlkjalkjsdlkajsdlkajskdljalkdjslkajsdlkjaskdjakljsdsa'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves(
                [
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":1
                         }
                      ]
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "count":0
                         }
                      ]
                   }
                ]
            );
        stubDB.onCall(1).resolves();
        stubDB.onCall(2).resolves(
                [
                   {
                      "command":"UPDATE"
                   },
                   {
                      "command":"SELECT",
                      "rows":[
                         {
                            "id_event_types":2
                         }
                      ]
                   }
                ]
            );

        return handlers.handleEvent(event).then(result => {
            expect(result[1].template.title).equal('belajar barengalkdjslakjsdlkajsdlkjal...');
        });
    });



});
