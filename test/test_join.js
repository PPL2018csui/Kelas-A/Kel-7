/* jshint expr: true */
'use strict';

var expect = require("chai").expect;
var sinon = require("sinon");
var line = require("../index.js");
var handlers = require("../handlers.js");
var client = line.client;
var func = require("../utils/utils_bot.js");

describe('testing insert command join', () =>{
    var stubDB;
    var stub;
    var stubProfileGroup;
    var stubProfileRoom;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
        stubDB = sinon.stub(func.database, "query");
        stubProfileGroup = sinon.stub(client, "getGroupMemberProfile");
        stubProfileRoom = sinon.stub(client, "getRoomMemberProfile");
    });

    afterEach(function(){
        stub.restore();
        stubDB.restore();
        stubProfileGroup.restore();
        stubProfileRoom.restore();
    });


    it("test join if fail to getProfile GROUP", () =>{
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubProfileGroup.rejects('gagal');
        const expected = {
            type: 'text',
            text: 'Terjadi error, silahkan coba lagi'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join if fail to getProfile ROOM", () =>{
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubProfileRoom.rejects('gagal');
        const expected = {
            type: 'text',
            text: 'Terjadi error, silahkan coba lagi'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join with argument if fail to getProfile GROUP", () =>{
        const msg = {
            type : 'text',
            text : '/join gagal cuy'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubProfileGroup.rejects('gagal');
        const expected = {
            type: 'text',
            text: 'Terjadi error, silahkan coba lagi'
        };

        handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join with argument if fail to getProfile ROOM", () =>{
        const msg = {
            type : 'text',
            text : '/join gagal cuy'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubProfileRoom.rejects('gagal');
        const expected = {
            type: 'text',
            text: 'Terjadi error, silahkan coba lagi'
        };

        handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it ("test insert command join 'laksjd' should return null", () => {
        const msg = {
            type : 'text',
            text : 'lakjsdlak'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        expect(handlers.handleEvent(event)).equals(null);
     });


    it ("test insert command join /joinakjshd should return null", () => {
        const msg = {
            type : 'text',
            text : '/joinlakjsdlak'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        expect(handlers.handleEvent(event)).equals(null);
     });

    it ("test insert command join should return join berhasil message GROUP", () => {
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubProfileGroup.resolves({displayName: 'Nama'});
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '1'}]});
        stubDB.onCall(1).resolves({rowCount : 1});
        const expected = {
            type: 'text',
            text: 'Nama berhasil join'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
     });

     it ("test insert command join should return join berhasil message ROOM", () => {
         const msg = {
             type : 'text',
             text : '/join'
         };
         const event = {
             type : 'message',
             replyToken : '123123',
             source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
             message : msg
         };
         stub.returnsArg(1);
         stubProfileRoom.resolves({displayName: 'Nama'});
         stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '1'}]});
         stubDB.onCall(1).resolves({rowCount : 1});
         const expected = {
             type: 'text',
             text: 'Nama berhasil join'
         };

         return handlers.handleEvent(event).then(result => {
             expect(result).include(expected);
         });
      });

      it ("test insert command join argumen should return join berhasil message GROUP", () => {
          const msg = {
              type : 'text',
              text : '/join 1_mungkin_terlambat'
          };
          const event = {
              type : 'message',
              replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
          };
          stub.returnsArg(1);
          stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '1'}]});
          stubDB.onCall(1).resolves({rowCount : 1});
          stubProfileGroup.resolves({displayName: 'Nama'});
          const expected = {
              type: 'text',
              text: 'Nama berhasil join'
          };

          return handlers.handleEvent(event).then(result => {
              expect(result).include(expected);
          });
       });

       it ("test insert command join should return join berhasil message ROOM", () => {
           const msg = {
               type : 'text',
               text : '/join 1_mungkin_terlambat'
           };
           const event = {
               type : 'message',
               replyToken : '123123',
             source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
             message : msg
           };
           stub.returnsArg(1);
           stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '1'}]});
           stubDB.onCall(1).resolves({rowCount : 1});
           stubProfileRoom.resolves({displayName: 'Nama'});
           const expected = {
               type: 'text',
               text: 'Nama berhasil join'
           };

           return handlers.handleEvent(event).then(result => {
               expect(result).include(expected);
           });
        });

    it("test insert command join with argument should return error pada server when checking event GROUP", () =>{
        const msg = {
            type : 'text',
            text : '/join 1laksjd'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.rejects('Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)');
        stubProfileGroup.resolves({displayName: 'Nama'});
        const expected = {
            type: 'text',
            text: 'Nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test insert command join with argument should return error pada server when checking event ROOM", () =>{
        const msg = {
            type : 'text',
            text : '/join 1laksjd'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.rejects('Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)');
        stubProfileRoom.resolves({displayName: 'Nama'});
        const expected = {
            type: 'text',
            text: 'Nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test insert command join should return join error pada server when checking event GROUP", () =>{
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.rejects('Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)');
        stubProfileGroup.resolves({displayName: 'Nama'});
        const expected = {
            type: 'text',
            text: 'Nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test insert command join should return join error pada server when checking event ROOM", () =>{
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.rejects('Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)');
        stubProfileRoom.resolves({displayName: 'Nama'});
        const expected = {
            type: 'text',
            text: 'Nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test insert command join with argument should return error pada server when inserting GROUP", () =>{
        const msg = {
            type : 'text',
            text : '/join 1laksjd'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '1'}]});
        stubDB.onCall(1).rejects('Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)');
        stubProfileGroup.resolves({displayName: 'Nama'});
        const expected = {
            type: 'text',
            text: 'Nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test insert command join with argument should return error pada server when inserting ROOM", () =>{
        const msg = {
            type : 'text',
            text : '/join 1laksjd'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '1'}]});
        stubDB.onCall(1).rejects('Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)');
        stubProfileRoom.resolves({displayName: 'Nama'});
        const expected = {
            type: 'text',
            text: 'Nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test insert command join should return join error pada server when inserting GROUP", () =>{
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '0'}]});
        stubDB.onCall(1).rejects('Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)');
        stubProfileGroup.resolves({displayName: 'Nama'});
        const expected = {
            type: 'text',
            text: 'Nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test insert command join should return join error pada server when inserting ROOM", () =>{
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '0'}]});
        stubDB.onCall(1).rejects('Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)');
        stubProfileRoom.resolves({displayName: 'Nama'});
        const expected = {
            type: 'text',
            text: 'Nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join if there's no event created GROUP", () =>{
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' belum bisa join karena belum ada event'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join if there's no event created ROOM", () =>{
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' belum bisa join karena belum ada event'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join with argument if there's no event created GROUP", () =>{
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join lakjsdlj'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' belum bisa join karena belum ada event'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join with argument if there's no event created ROOM", () =>{
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join lakjsdlj'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' belum bisa join karena belum ada event'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join if already join GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '0'}]});
        stubDB.onCall(1).rejects({code : '23505'});
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' sudah terdaftar sebelumnya'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join if already join ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '0'}]});
        stubDB.onCall(1).rejects({code : '23505'});
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' sudah terdaftar sebelumnya'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join with argument if already join GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join lakjsdlj'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '1'}]});
        stubDB.onCall(1).rejects({code : '23505'});
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' sudah terdaftar sebelumnya'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join with argument if already join ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join lakjsdlj'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{admin_id : 'adminidtest', id_event_types: '1'}]});
        stubDB.onCall(1).rejects({code : '23505'});
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' sudah terdaftar sebelumnya'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit berhasil GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubProfileGroup.resolves({displayName: 'Nama'});
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{option_name : 'opsi1', id_event_types: '2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1, rows: [{option_name : 'opsi1'}]}); //check optionnya ada atau tidak
        stubDB.onCall(2).resolves({rowCount : 1}); // insert ke partisipan
        const expected = {
            type: 'text',
            text: 'Nama memilih opsi1'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });

    });

    it("test join chooseit if already join berhasil GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types: '2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1, rows: [{option_name : 'opsi2'}]}); //check optionnya ada atau tidak
        stubDB.onCall(2).rejects({code : '23505'}); //error berarti udah pernah memilih
        stubDB.onCall(3).resolves({rowCount : 1}); //update ganti pilihan
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' mengganti pilihan menjadi opsi2'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit if already join gagal update opsi GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1, rows: [{option_name : 'opsi2'}]}); //check optionnya ada atau tidak
        stubDB.onCall(2).rejects({code : '23505'}); //error berarti udah pernah memilih
        stubDB.onCall(3).rejects({rowCount : 1}); //update ganti pilihan
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit jika opsi tidak ada pada pilihan GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_99'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types: '2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 0}); // check optionnya ada atau tidak
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Opsi tersebut tidak ada pada pilihan.'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit if there's no event created GROUP", () =>{
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' belum bisa join karena belum ada event'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit failed jika opsi tidak ada pada pilihan GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_99'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types: '2'}]}); //check exist event
        stubDB.onCall(1).rejects({rowCount : 0}); // check optionnya ada atau tidak
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit failed join GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1, rows: [{option_name : 'opsi2'}]}); //check optionnya ada atau tidak
        stubDB.onCall(2).rejects({rowCount: 1}); //error berarti udah pernah memilih
        stubDB.onCall(3).rejects({rowCount : 1}); //update ganti pilihan
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit failed join jika event bukan chooseit GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'1'}]}); //check exist event
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Event yang telah dibuat bukan Wacana Kolektif.'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    //--------------------------------------
    //ROOM BUAT CHOOSEIT
    //-----------------------------------

    it("test join chooseit berhasil ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubProfileRoom.resolves({displayName: 'Nama'});
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{option_name : 'opsi1', id_event_types: '2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1, rows: [{option_name : 'opsi1'}]}); //check optionnya ada atau tidak
        stubDB.onCall(2).resolves({rowCount : 1}); // insert ke partisipan
        const expected = {
            type: 'text',
            text: 'Nama memilih opsi1'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });

    });

    it("test join chooseit if already join berhasil ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types: '2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1, rows: [{option_name : 'opsi2'}]}); //check optionnya ada atau tidak
        stubDB.onCall(2).rejects({code : '23505'}); //error berarti udah pernah memilih
        stubDB.onCall(3).resolves({rowCount : 1}); //update ganti pilihan
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' mengganti pilihan menjadi opsi2'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit if already join gagal update opsi ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1, rows: [{option_name : 'opsi2'}]}); //check optionnya ada atau tidak
        stubDB.onCall(2).rejects({code : '23505'}); //error berarti udah pernah memilih
        stubDB.onCall(3).rejects({rowCount : 1}); //update ganti pilihan
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit jika opsi tidak ada pada pilihan ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_99'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types: '2'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 0}); // check optionnya ada atau tidak
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Opsi tersebut tidak ada pada pilihan.'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit if there's no event created ROOM", () =>{
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_1'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 0});
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: nama + ' belum bisa join karena belum ada event'
        };

        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit failed jika opsi tidak ada pada pilihan ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_99'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types: '2'}]}); //check exist event
        stubDB.onCall(1).rejects({rowCount : 0}); // check optionnya ada atau tidak
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join chooseit failed join jika event bukan chooseit ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join_2'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'1'}]}); //check exist event
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Event yang telah dibuat bukan Wacana Kolektif.'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });
    //-----------------------------------------------------------------
    it("test join argumen jika event tanpa argumen ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join test argumen'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'0'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1}); //berhasil insert
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama berhasil join tetapi info tambahan tidak didaftarkan karena berjenis Wacana Tunggal Sederhana.'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join argumen jika event tanpa argumen insert failed ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join test argumen'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'0'}]}); //check exist event
        stubDB.onCall(1).rejects({rowCount : 0}); //berhasil insert
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join argumen jika event tanpa argumen GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join test argumen'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'0'}]}); //check exist event
        stubDB.onCall(1).resolves({rowCount : 1}); //berhasil insert
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama berhasil join tetapi info tambahan tidak didaftarkan karena berjenis Wacana Tunggal Sederhana.'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join argumen jika event tanpa argumen insert failed GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join test argumen'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'0'}]}); //check exist event
        stubDB.onCall(1).rejects({rowCount : 0}); //berhasil insert
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama, Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join argumen jika event tanpa argumen insert sudah ada di list GROUP", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join test argumen'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'0'}]}); //check exist event
        stubDB.onCall(1).rejects({code : '23505'}); //berhasil insert
        stubProfileGroup.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama sudah terdaftar sebelumnya'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join argumen jika event tanpa argumen insert sudah ada di list ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join test argumen'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'0'}]}); //check exist event
        stubDB.onCall(1).rejects({code : '23505'}); //berhasil insert
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: 'nama sudah terdaftar sebelumnya'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });

    it("test join argumen jika event tanpa argumen insert saat event adalah chooseit ROOM", () => {
        const nama = 'nama';
        const msg = {
            type : 'text',
            text : '/join'
        };
        const event = {
            type : 'message',
            replyToken : '123123',
            source : {userId : 'kakjshdouahs', roomId : 'adjaowdjkwad'},
            message : msg
        };
        stub.returnsArg(1);
        stubDB.onCall(0).resolves({rowCount : 1, rows: [{id_event_types:'2'}]}); //check exist event
        stubDB.onCall(1).rejects({code : '23505'}); //berhasil insert
        stubProfileRoom.resolves({displayName: nama});
        const expected = {
            type: 'text',
            text: "nama harus memilih opsi karena event adalah Wacana Kolektif. Contoh '/join_1'"
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });
});
