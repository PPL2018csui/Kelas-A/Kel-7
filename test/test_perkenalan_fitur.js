/* jshint expr: true */
'use strict';

var expect = require("chai").expect;
var chai = require("chai");
var sinon = require("sinon");
var line = require("../index.js");
var handlers = require("../handlers.js");
var client = line.client;
var func = require("../utils/utils_bot.js");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
chai.should();

describe('testing intro message', () => {

    var stub;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
    });

    afterEach(function(){
        stub.restore();
    });

    it("should return a greeting message to personal chat", () => {
        const event = {
            type: 'follow',
            replyToken: '123123'
        };
        stub.returnsArg(1);
        const a = handlers.handleEvent(event);
        expect(a[0].text).equal("Hai! Terima kasih telah mengundang saya. Mulai interaksi dengan memanggil saya. Panggil 'agus'");
        expect(a[1].template).property("type", "buttons");
    });

    it ("should return a greeting message to group chat", () => {

        const event = {
            replyToken: 'nHuyWiB7yP5Zw52FIkcQobQuGDXCTA',
            type: 'join',
            timestamp: 1462629479859,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad', type : 'group'},
        };
        stub.returnsArg(1);
        const a = handlers.handleEvent(event);
        expect(a[0].text).equal("Hai! Terima kasih telah mengundang saya. Mulai interaksi dengan memanggil saya. Panggil 'agus'");
    });

    it ("should return a greeting message to multi chat", () => {

        const event = {
            replyToken: 'nHuyWiB7yP5Zw52FIkcQobQuGDXCTA',
            type: 'join',
            timestamp: 1462629479859,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad', type : 'room'},
        };
        stub.returnsArg(1);
        const a = handlers.handleEvent(event);
        expect(a[0].text).equal("Hai! Terima kasih telah mengundang saya. Mulai interaksi dengan memanggil saya. Panggil 'agus'");
    });
});

describe('testing no action', ()=>{
    var stub;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
    });

    afterEach(function(){
        stub.restore();
    });

    it ('Should ignore a non command', () => {
        const msg = {
            type : 'text',
            text : 'jAHSIuadhquwdh'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            message : msg,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
        };
        const a = handlers.handleEvent(event);
        expect(a).equals(null);
    });

});

describe('testing help message', () => {
    var stub;
    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stub.returnsArg(1);
    });

    afterEach(function(){
        stub.restore();
    });

    it('should return a carousel when agus is called', () => {
        const msg = {
            type : 'text',
            text : 'agus'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            message : msg,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
        };

        stub.returnsArg(1);
        const m = handlers.handleEvent(event);
        expect(m.template.type).equal('carousel');
    });

    it ("should return 'Pesan help' message", () => {
        const msg = {
            type : 'text',
            text : '/help'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            message : msg,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
        };

        stub.returnsArg(1);
        const m = handlers.handleEvent(event);
        var expectedString = 'Saya bisa apa aja ya?\n'+
                                '\n'+
                                'Untuk event:\n'+
                                '-Ketik /create_event untuk membuat event\n'+
                                '-Ketik /join untuk join event\n'+
                                '-Ketik /cancel untuk membatalkan partisipasi\n'+
                                '-Ketik /remove untuk menghapus event yg sudah ada\n'+
                                '\n'+
                                'Jika Kamu ingin agus keluar dari grup (semoga untuk sesaat saja), ketik /bye';
        expect(m.text).equal(expectedString);
    });

    it('should return pesan listit help', () =>{
        const msg = {
            type : 'text',
            text : '/help tunggal'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            message : msg,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'},
        };

        stub.returnsArg(1);
        const m = handlers.handleEvent(event);

        var expected = "Ngebuat wacana buat ngelist doang\n"+
                "Yang kaya begini nih:\n\n"+
                "Yang ikut Wacana:\n"+
                "1. -\n"+
                "2. -\n"+
                "3. -\n\n"+
                "*Note : Kalo cuma mau ngedata, pilih yang tanpa argumen, kalo mau ada catetan-catetannya, pilih yang dengan argumen"
                ;
        expect(m.text).equal(expected);
    });
    
    it ("should return 'Pesan help chooseIt' message", () => {
        const msg = {
            type : 'text',
            text : '/help kolektif'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            message : msg,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'}
        };

        stub.returnsArg(1);
        const m = handlers.handleEvent(event);
        var expectedString = "Ngebuat wacana yang punya opsi\n"+
                         "Yang kaya begini nih:\n\n"+
                         "Minggu Depan Belajar Bareng Kuy:\n"+
                         "-Selasa:\n"+
                         "  1. Adi\n"+
                         "  2. Budi\n"+
                         "-Kamis\n"+
                         "  1. Susi";
        expect(m.text).equal(expectedString);
    });

    it("menampilkan string panduan listIt", () => {
        const msg = {
            type : 'text',
            text : '/help listit_1'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            message : msg,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'}
        };
	
		stub.returnsArg(1);
		const m = handlers.handleEvent(event);

		var expected = "Ngebuat wacana yang ngelist nama akun LINE\n"+
                "Yang kaya begini nih:\n\n"+
                "Yang ikut Wacana:\n"+
                "1. Adi\n"+
                "2. Budi";
		expect(m.text).equal(expected);
	});


    it("menampilkan string panduan listIt", () => {
        const msg = {
            type : 'text',
            text : '/help listit_2'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            message : msg,
            source : {userId : 'kakjshdouahs', groupId : 'adjaowdjkwad'}
        };
    
        stub.returnsArg(1);
        const m = handlers.handleEvent(event);

        var expected = "Ngebuat wacana yang ngelist nama akun LINE sama catetan-catetannya\n"+
                "Yang kaya begini nih:\n\n"+
                "Cantumin Nomor Hp Dong:\n"+
                "1. Adi - 08xxx\n"+
                "2. Budi - 02xxx";
        expect(m.text).equal(expected);
    });

});

describe('testing bot leave group', () =>{
    var stubDB;
    var stub;
    var stubLeave;

    beforeEach(function(){
        stub = sinon.stub(client, "replyMessage");
        stubDB = sinon.stub(func.database, "query");
        stubLeave = sinon.stub(client, "leaveGroup");
    });

    afterEach(function(){
        stub.restore();
        stubDB.restore();
        stubLeave.restore();
    });

    it ("should success for the bot to leave", () =>{
       const msg = {
            type : 'text',
            text : '/bye'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {groupId : 'kakjshdouahs'},
            message : msg
        };

        stubDB.resolves();
        stub.resolves();
        stubLeave.resolves();

        const expected = {
            type : 'text',
            text : 'da dah'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include("berhasil");
        });
    });

    it ("should fail to leave", ()=>{
        const msg = {
            type : 'text',
            text : '/bye'
        };
        const event = {
            type : 'message',
            replyToken : '123',
            source : {groupId : 'kakjshdouahs'},
            message : msg
        };

        stubDB.resolves();
        stub.onCall(0).resolves();
        stubLeave.rejects();
        stub.onCall(1).returnsArg(1);

        const expected = {
            type : 'text',
            text : 'terjadi error pada server, coba beberapa saat lagi'
        };
        return handlers.handleEvent(event).then(result => {
            expect(result).include(expected);
        });
    });
});
