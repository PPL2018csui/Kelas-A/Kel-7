const line = require("./index.js");
const utils_bot = require("./utils/utils_bot.js");
const utils_remove = require("./utils/utils_remove.js");
const utils_create_listit = require("./utils/utils_create_listit.js");
const utils_show = require("./utils/utils_show.js");
const utils_cancel = require("./utils/utils_cancelParticipation.js");
const utils_join = require("./utils/utils_join.js");
const {ErrorDB, SelectionError} = require('./custom/agus_error.js');
const utils_feat_intro = require('./utils/utils_feature_introductions.js');
const utils_join_chooseit = require("./utils/utils_join_chooseit.js");
const utils_create_chooseit = require("./utils/utils_create_chooseit.js");

exports.handleEvent = function(event) {

 if(event.type == 'join' && (event.source.type == 'group' || event.source.type == 'room')){
	 var val = utils_feat_intro.join_group();
	 console.log(val);
   return line.client.replyMessage(event.replyToken, val);
}

else if(event.type == 'follow') {
	console.log('masuk kok');
  var intro = utils_feat_intro.introPersonal();
	return line.client.replyMessage(event.replyToken, intro);
}

else {
		var UNDER_CONSTRUCTION_URL="https://image.ibb.co/i5pbQS/under_construction_2408066_640.jpg";
		var BACKUP_MESSAGE = {type: 'image', originalContentUrl:UNDER_CONSTRUCTION_URL, previewImageUrl:UNDER_CONSTRUCTION_URL};
		var groupId = event.source.groupId || event.source.roomId;

		if (event.message.text == '/help') {
          var help = utils_feat_intro.helpMessage();
          return line.client.replyMessage(event.replyToken, help);
      	}
		
		else if(event.message.text == '/help kolektif') {
			var helpChooseIt = utils_feat_intro.helpChooseIt();
			return line.client.replyMessage(event.replyToken, helpChooseIt);
		}
		
		else if(event.message.text == '/help tunggal') {
			var helpListIt = utils_feat_intro.helpListIt();
			return line.client.replyMessage(event.replyToken, helpListIt);
		}

		else if(event.message.text == '/help listit_1') {
			var helpListIt1 = utils_feat_intro.helpListItWithoutArgument();
			return line.client.replyMessage(event.replyToken, helpListIt1);
		}

		else if(event.message.text == '/help listit_2') {
			var helpListIt2 = utils_feat_intro.helpListItWithArgument();
			return line.client.replyMessage(event.replyToken, helpListIt2);
		}

		else if(event.message.text == 'agus') {
			var menu = utils_feat_intro.menu();
			return line.client.replyMessage(event.replyToken, menu);
		}
		
		else if(event.message.text == '/show'){
			if (event.source.groupId){
				return utils_show.showAllParticipants(groupId, event.source.userId).then(function(result){
					return line.client.replyMessage(event.replyToken, result);
				});
			}else{
				return utils_show.showAllParticipants(groupId, event.source.userId).then(function(result){
					return line.client.replyMessage(event.replyToken, result);
				});
			}

		}

		else if(event.message.text == '/remove'){
			return utils_remove.removeVerifyGroup(groupId).then(function(result){
				return line.client.replyMessage(event.replyToken, result);
			});
		}

		else if(event.message.text == '/remove confirm'){
			return utils_remove.removeReverifyGroupAfterConfirmation(groupId , event.source.userId).then(function(result){
				return line.client.replyMessage(event.replyToken, result);
			});
		}
		else if (event.message.text == '/bye'){

			return utils_bot.leaveGrup(groupId).then(function(result) {
				echo = {type: 'sticker', packageId: '1', stickerId: '408'};
				return line.client.replyMessage(event.replyToken, echo).then(() =>{
					return line.client.leaveGroup(event.source.groupId).then(() => {
						return "berhasil";
					}).catch((err) => {
						echo = {type: 'text', text: 'terjadi error pada server, coba beberapa saat lagi'};
						return line.client.replyMessage(event.replyToken, echo);
					});
				});
			});
		}

		else if(event.message.text == '/cancel'){
			var user_id = event.source.userId;
			var getProfileFunction;

			if(event.source.groupId){
				getProfileFunction = function(groupId, user_id){
					return line.client.getGroupMemberProfile(groupId, user_id);
				};
			}else{
				getProfileFunction = function(groupId, user_id){
					return line.client.getRoomMemberProfile(groupId, user_id);
				};
			}
				return getProfileFunction(groupId, user_id).then((profile) => {
					var echo = {type: 'text', text: profile.displayName + ' membatalkan partisipasi'};
					return utils_cancel.cekExistedEventCancel(groupId).then(function(result){

		      			if(result instanceof SelectionError){
							echo = {type: 'text', text: profile.displayName + ', anda tidak bisa cancel karena event belum dibuat'};
							return line.client.replyMessage(event.replyToken, echo);
						}

						if(result instanceof ErrorDB){
							echo = {type: 'text', text: profile.displayName + ', Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'};
							return line.client.replyMessage(event.replyToken, echo);
						}

						return utils_cancel.cekExistedParticipantCancel(user_id, groupId).then(function(result){
								if(result.type == '6'){
									echo = {type: 'text', text: profile.displayName + ', ' + result.text};
									return line.client.replyMessage(event.replyToken, echo);
								} else if(result.type == '4'){
									echo = {type: 'text', text: profile.displayName + ', ' + result.text};
									return line.client.replyMessage(event.replyToken, echo);
								} else if(result.type == '8'){
									echo = {type: 'text', text: profile.displayName + ', ' + result.text};
									return line.client.replyMessage(event.replyToken, echo);
								}
								else{
									return utils_cancel.cancelJoinEvent(user_id).then(function(result){
										if(result.type == '1'){
											return line.client.replyMessage(event.replyToken, echo);
										} else{
											echo = {type: 'text', text: profile.displayName + ', ' + result.text};
											return line.client.replyMessage(event.replyToken, echo);
										}
									});
								}
						});

					});
				}).catch((err) => {
					var echo = {
						type: 'text', text: 'Tambahkan agus sebagai teman sebelum cancel'
					};
					return line.client.replyMessage(event.replyToken, echo);
				});
		}

		else if(/^\/join.*$/.test(event.message.text)){
	      var input = event.message.text.split(" ");
	      var userId = event.source.userId;
          var inputChooseit = event.message.text.split("_");
	      if(input.length == 1 && input[0] == '/join'){
	      	input.splice(0, 1);
		    input = input.join(" ");
		    inputChooseit = input[1];
			/**
			buat dapetin display name group tanpa harus add pake yang instruksi
			https://developers.line.me/en/docs/messaging-api/reference/#group
			*/
			if(event.source.groupId){
			    return line.client.getGroupMemberProfile(groupId, userId).then((profile) => {
			    	return utils_join.joinTanpaArgumen(groupId, userId, profile).then((message) => {
						return line.client.replyMessage(event.replyToken, message);
					});

			      }).catch((err) => {
			        	console.log(err);
			        	var echo = {
			          		type: 'text', text: 'Terjadi error, silahkan coba lagi'
			        	};
			        return line.client.replyMessage(event.replyToken, echo);
			      });
			  }else{
				  return line.client.getRoomMemberProfile(groupId, userId).then((profile) => {
					  return utils_join.joinTanpaArgumen(groupId, userId, profile).then((message) => {
						  return line.client.replyMessage(event.replyToken, message);
					  });
  			      }).catch((err) => {
  			        	console.log(err);
  			        	var echo = {
  			          		type: 'text', text: 'Terjadi error, silahkan coba lagi'
  			        	};
  			        return line.client.replyMessage(event.replyToken, echo);
  			      });
			  }
	      }
	      else if((input[0] == '/join' && input.length > 1) || inputChooseit[0] == '/join' && inputChooseit.length == 2){
	      	//dengan argumen dan chooseit
		      input.splice(0, 1);
		      input = input.join(" ");
			  var getProfile;
			  if(event.source.groupId){
				  getProfile = function(groupId, userId){
					  return line.client.getGroupMemberProfile(groupId, userId);
				  };
              }else{
                  getProfile = function(groupId, userId){
					  return line.client.getRoomMemberProfile(groupId, userId);
				  };
              }
			  return getProfile(groupId, userId).then((profile) => {
				 	return utils_join.joinArgumenDanChooseIt(groupId, userId, profile, input, inputChooseit).then((message) =>{
						return line.client.replyMessage(event.replyToken, message);
					});
			  }).catch((err) => {
  			   		console.log(err);
  					var echo = {
  					  type: 'text', text: 'Terjadi error, silahkan coba lagi'
  					};
  				   	  return line.client.replyMessage(event.replyToken, echo);
  			  });
		  }
	      else{
	      	return null;
	      }
	    }

	    else if (/^\/event_name .*$/.test(event.message.text)){
	    	var messageEventName = event.message.text.split(" ");
	    	messageEventName.splice(0,1);
            var eventName = messageEventName.join(" ");
            return utils_create_listit.setUpListItName(groupId, event.source.userId, eventName).then(function(result){
	    		return line.client.replyMessage(event.replyToken, result);
	    	});

	    }

	    else if (event.message.text == '/event_name'){
			message = { type: 'text', text: 'Nama event belum ditulis\n' +
											'Contoh: /event_name belajar besok'
					};
			return line.client.replyMessage(event.replyToken, message);
	    }

	    else if (event.message.text == '/create_event'){
	    	return utils_create_listit.createEvent(groupId).then(function(result){
	    		return line.client.replyMessage(event.replyToken, result);
	    	});

	    }

    	else if (/^\/create.*$/.test(event.message.text)){
        	if(event.message.text == '/create'){
	            message = { type: 'text', text: 'Jenis event belum ditulis'};
	            return line.client.replyMessage(event.replyToken, message);
          	}

          	else if(event.message.text == '/create listit'){

            	return utils_create_listit.chooseEventListIt(groupId).then(function(result){
		  			return line.client.replyMessage(event.replyToken, result);
		  		});

		  	}else if(event.message.text == '/create listit_1'){

		  		return utils_create_listit.setUpListItType(groupId, 0).then(function(result){
		  			return line.client.replyMessage(event.replyToken, result);
		  		});

		  	}else if(event.message.text == '/create listit_2'){

		  		return utils_create_listit.setUpListItType(groupId, 1).then(function(result){
		  			return line.client.replyMessage(event.replyToken, result);
		  		});

		  	}else if(event.message.text == '/create chooseit'){
				return utils_create_chooseit.setUpChooseItType(groupId, event.source.userId, 2).then(function(result){
					return line.client.replyMessage(event.replyToken, result);
				});

		  	} else if(/^\/create chooseit_[1-9]$/.test(event.message.text)){

		  		const inp = event.message.text;
		  		const jumlahOpsi = inp.substr(inp.length - 1);
		  		if (jumlahOpsi > 4){
		  			message = {type: 'text', text: 'Jumlah maksimal opsi hanya 4'};
		  			return line.client.replyMessage(event.replyToken, message);
		  		}
		  		return utils_create_chooseit.insertOptions(groupId, jumlahOpsi, 0).then(function(result){
	  				return line.client.replyMessage(event.replyToken, result);
		  		});
		  	} else{
              message = { type: 'text', text: 'Command tidak dikenal'};
              return line.client.replyMessage(event.replyToken, message);
            }

        } else if(/^\/opsi[1-9] .*$/.test(event.message.text)){
        	var inp = event.message.text;
		  	inp = inp.split(" ");
		  	var opsiKe = inp[0];
		  	opsiKe = opsiKe.slice(-1);
		  	inp.splice(0, 1);
		    inp = inp.join(" ");

		  	return utils_create_chooseit.startInsertOption(groupId, opsiKe, inp).then(function(result){
		  		return line.client.replyMessage(event.replyToken, result);
		  	});
        } else if(event.message.text == '/opsi confirm'){
        	return utils_create_chooseit.confirmOptions(groupId).then(function(result){
        		return line.client.replyMessage(event.replyToken, result);
        	});
        }

	    else {
	    	return null;
	    }
  }
};
