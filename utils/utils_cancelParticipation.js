const utils = require('./utils_bot.js');
const {ErrorDB, SelectionError} = require('../custom/agus_error.js');

/**
* Template fungsi
**/

// export.namafunction = function(){
// 	return sesuatu
// }

exports.cekExistedEventCancel = function(groupId){
	const query = "SELECT FROM agus.event where group_id = '" + groupId + "' AND is_active='t'";
	return utils.executeQuery(query, cekExistedEventCancelHandler);
};

exports.cekExistedParticipantCancel = function(userId, groupId){
	const query = "SELECT FROM agus.participant where user_id = '" + userId +"' AND group_id = '" + groupId + "';\n" +
					"SELECT id_event_types from agus.event WHERE group_id = '" + groupId + "';";
	return utils.executeQuery(query, cekExistedParticipantCancelHandler);
};

var cekExistedEventCancelHandler = function(dbResult){

	if(dbResult.result.rowCount  == 1){
		return {type: '1', text: 'berhasil select'};
	} else{
		if (dbResult.result.rowCount == 0){
			return new SelectionError("Error RowCount 0");
		} else{
			return new ErrorDB("Error form server");
		}
	}
	
};

var cekExistedParticipantCancelHandler = function(dbResult){
	if(dbResult.status == 'ok'){
		if(dbResult.result[0].rowCount == '1'){
			return {type: '1', text: 'berhasil select'};
		} else{
			if (dbResult.result[1].rowCount == '1'){
				var eventType = dbResult.result[1].rows[0].id_event_types;
				if(eventType == '2'){
					return {type: '8', text: 'anda tidak bisa cancel karena belum memilih opsi apapun'};
				}
				return {type: '6', text: 'anda tidak bisa cancel karena belum join'};
			} else{
				return {type: '4', text:'gagal cancel karena event tidak ada'};
			}
		}
	}else{
		return {type: '4', text:'Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'};
	}
	
};

exports.cancelJoinEvent = function(userId){
	const query = "DELETE FROM agus.participant WHERE user_id ='" + userId + "'";
	return utils.executeQuery(query, cancelJoinEventHandler);
};



var cancelJoinEventHandler = function(dbResult){
	if(dbResult.result.rowCount == '1'){
		return {type: '1', text: 'berhasil cancel'};
	}else {
		return {type: '4', text:'Terjadi suatu kesalahan pada server saat ingin cancel event. Harap coba beberapa saat lagi'};
	}
	
};
