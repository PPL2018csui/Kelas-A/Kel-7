const utils_bot = require("../utils/utils_bot.js");



exports.removeVerifyGroup = function(groupId){

	var query = "SELECT FROM agus.event WHERE group_id ='" + groupId + "';";
	return utils_bot.executeQuery(query, removeVerifiedGroup, {groupId : groupId});  
};

var removeVerifiedGroup = function(dbResult){

	if(dbResult.status == 'ok' && dbResult.result.rowCount == 1){
		var removeButton = {
			
			  "type": "template",
			  "altText": "kirim '/remove confirm' untuk mengkonfirmasi penghapusan",
			  "template": {
			    "type": "buttons",
			    "actions": [
			      {
			        "type": "message",
			        "label": "Ya",
			        "text": "/remove confirm"
			      }
			    ],
			    "text": "Anda akan menghapus event. Apakah anda yakin?"
			  }
			

		}; 

		return removeButton;
	} else {
		var notification = {
			type: 'text',
            text: 'Tidak ada event yang dibuat'
		};
		return notification;
	}
};


exports.removeReverifyGroupAfterConfirmation = function(groupId, userId){

	var query = "SELECT FROM agus.event WHERE group_id ='" + groupId + "';";
	return utils_bot.executeQuery(query, removeVerifiedRemoval, {groupId : groupId, userId : userId});  

};

// var removeReverifyAdminAfterConfirmation = function(dbResult){

// 	if(dbResult.status == 'ok' && dbResult.result.rowCount == 1){
// 		var groupId = dbResult.additionalArg.groupId;
// 		var userId = dbResult.additionalArg.userId;
// 		var query = "SELECT FROM agus.event WHERE group_id ='" + groupId + "' AND admin_id='"+ userId +"';";
// 		return utils_bot.executeQuery(query, removeVerifiedRemoval, {groupId : groupId, userId : userId});
// 	} else if(dbResult.status == 'ok' && dbResult.result.rowCount == 0){
// 		var notification = {
// 			type: 'text',
//             text: 'Tidak ada event yang dibuat'
// 		};
// 		return notification;
// 	} else {
// 		var failReport = {
// 			type: 'text',
//             text: 'Proses verifikasi event gagal. Silahkan coba lagi!'
// 		};
// 		return failReport;
// 	}

// };

var removeVerifiedRemoval = function(dbResult){
	
	if(dbResult.status == 'ok' && dbResult.result.rowCount == 1){
		var groupId = dbResult.additionalArg.groupId;
		var userId = dbResult.additionalArg.userId;
		var query = "DELETE FROM agus.event WHERE group_id='" + groupId + "';";
		return utils_bot.executeQuery(query, removeSuccess);
	} else if(dbResult.status == 'ok' && dbResult.result.rowCount == 0){
		var notification = {
			type: 'text',
            text: 'Tidak ada event yang dibuat'
		};
		return notification;
	} 
	else {
		var failReport = {
			type: 'text',
            text: 'Penghapusan gagal pada server. Silahkan tunggu kemudian coba lagi.'
		};
		return failReport;
	}
	  
};

var removeSuccess = function(dbResult){
	if(dbResult.status == 'ok'){
		return {type: 'text', text: 'Event berhasil dihapus.'};
	} 
	else {
		var notification = {
			type: 'text',
            text: 'Penghapusan gagal pada server. Silahkan tunggu kemudian coba lagi.'
		};
		return notification;
	}
};

