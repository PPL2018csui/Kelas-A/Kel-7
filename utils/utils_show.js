const utils_bot = require("./utils_bot.js");

var EVENT_TYPE_LIST_IT_NO_ARGS = 0;
var EVENT_TYPE_CHOOSE_IT = 2;

var NO_EVENT_BUTTON = {
	type: 'template',
	altText: 'Ketik /create_event untuk membuat event',
	template: {
		type: 'buttons',
		text: 'Buat event?',
		actions: [
			{
				type: 'message',
				label: 'Ya',
				text: '/create_event'
			}
		]
	}
};

var NO_EVENT_MESSAGE = {
	type: 'text',
	text: 'Belum ada event atuh bro.....\nketik /create_event untuk buat event baru'
};

var text = {
    true: 'Anda sudah join, mau membatalkan? ketik /cancel atau tekan tombol di bawah!',
    false: 'Anda belum join, mau join? ketik /join atau tekan tombol di bawah!'
};

var actions = {
    false: '/join',
    true:'/cancel'
};

var hasEvent = function(groupId) {
	var query = "SELECT e.event_name, e.id_event_types FROM agus.event e WHERE e.is_active = 't' and e.group_id = '" + groupId + "';";
	return utils_bot.executeQuery(query, utils_bot.selectTableHandler).then(function(result) {

		if(result.type == '1') {
			return {type: '1', res:result.rows};
		} else {
			return {type: '2', res:'Koneksi bermasalah'};
		}
	});
};

exports.getEveryOptionName = function(groupId) {
	var query = "select o.id_option, o.option_name from agus.options o where o.group_id = '"+groupId+"' and o.is_active=true;";
	return utils_bot.executeQuery(query, utils_bot.selectTableHandler).then(function(result) {
		return {type: '1', res: result.rows};
	});
};

var formMessage = function(listAndUserJoinStatus, result, event_type, optionsArray) {
	var template;
	if(event_type == EVENT_TYPE_CHOOSE_IT){
		template = module.exports.buildChooseItButton(optionsArray);
	}else{
		template = buildParticipantTemplate(listAndUserJoinStatus.userHasJoined);
	}
	var message = {type: 'text', status:result.type, text:listAndUserJoinStatus.list};

	res = [message];
	if(event_type == 2){
		res.push(template[1]);
	}else if(event_type != 1) {
		res.push(template);
	}
	return res;
};

exports.showAllParticipants = function (groupId, userId) {
	const command = "SELECT * from agus.participant p where p.group_id = '" + groupId +"'";
	const query = command;
	return hasEvent(groupId).then(function(eventExist) {
		if(eventExist.type == '2') {
			return {type: 'text', text:eventExist.res};
		}
		if(eventExist.res.length > 0) {
			var eventName = eventExist.res[0].event_name;
			var eventType = eventExist.res[0].id_event_types;
			return utils_bot.executeQuery(query, utils_bot.selectTableHandler).then(function(result) {
				var res;
				var listAndUserJoinStatus;
				if (eventType == EVENT_TYPE_CHOOSE_IT) {
					return module.exports.getEveryOptionName(groupId).then(function(res) {
						listAndUserJoinStatus = makeParticipantsListWithOptions(result.rows, userId, eventName, groupId, res.res);
						return formMessage(listAndUserJoinStatus, result, eventType, res.res);
					});
				} else {
					listAndUserJoinStatus = makeParticipantsList(result.rows, userId, eventName);
					return formMessage(listAndUserJoinStatus, result, eventType);
				}
			});
		} else {
			return [NO_EVENT_MESSAGE, NO_EVENT_BUTTON];
		}
	});

};
exports.buildChooseItButton = function(optionsArray){
	var listOpt = 'Silahkan pilih opsi dengan menulis perintah dibawah ini!';
	var actionOpsi = [];
	var index = 0;
	optionsArray.forEach(function(element) {
		var name;
		listOpt = listOpt + "\n /join_" + element.id_option + " -> ";
		if(element.option_name.length > 75){
			name = element.option_name.slice(0,75) + "...";
			listOpt = listOpt + name;
		}else{
			name = element.option_name;
			listOpt = listOpt + name;
		}
		if(element.option_name.length > 20){
			name = element.option_name.slice(0,17)+"...";
		}else{
			name = element.option_name;
		}
		actionOpsi[index] = {
								title: 'Opsi ' + (index+1),
								text: name,
								actions: [
									{"type": "message", "label" : 'Join', "text": "/join_"+element.id_option}
								]
							};
		index++;
	}, this);

	var picture = {
		type: "image",
		originalContentUrl: 'https://image.ibb.co/iJB90o/wacana_kolektif.png',
		previewImageUrl: 'https://image.ibb.co/iJB90o/wacana_kolektif.png'
	};


	var button = {
		type: "template",
		altText: listOpt,
		template: {
			type: "carousel",
			columns: actionOpsi
			//text : "Silahkan pilih opsi dengan menekan tombol dibawah!"
		}
	};
	return [picture, button];
};


var buildParticipantTemplate = function(userJoinStatus){

	var templateText = text[userJoinStatus];
	var templateAction = actions[userJoinStatus];

	var template = {
		type: "template",
		altText: templateText,
		template: {
				type: "buttons",
				text: templateText,
				actions: [
					{
						type: 'message',
						label: 'Ya',
						text: templateAction
					}
				]
		}
	};

	return template;
};

var makeParticipantsList = function(participantsArray, userId, eventName) {
	var userHasJoined = false;
	var res = "Yang ikutan '" + eventName + "'";
	counter = 0;
	participantsArray.forEach(function(element) {
		counter++;
		res = res + "\n"+ counter + ". " + element.nama_partisipan;
		if(element.user_comment != null) {
			res = res + " - " + element.user_comment;
		}
		if(element.user_id == userId) {
			userHasJoined = true;
		}
	}, this);
	return {list: res, userHasJoined: userHasJoined};
};

var makeParticipantsListWithOptions = function(participantsArray, userId, eventName, groupId, options) {
	var userHasJoined = false;
	var list = eventName + ":\n";
	options.forEach(function(option) {
		var counter = 0;
		list = list + "*" + option.option_name + ":\n";
		participantsArray.forEach(function(participant) {
			if(option.id_option == participant.id_option) {
				counter = counter + 1;
				list = list + "   " + counter + ". " + participant.nama_partisipan + "\n";
				if(participant.user_id == userId) {
					userHasJoined = true;
				}
			}
		});
	});

	return {list: list, userHasJoined: userHasJoined};
};
