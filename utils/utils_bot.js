const { Pool, Client } = require('pg');
const url = require('url');
const params = url.parse(process.env.DATABASE_URL);
const auth = params.auth.split(':');

const client = new Client({
  user: auth[0],
  password: auth[1],
  host: params.hostname,
  port: params.port,
  database: params.pathname.split('/')[1],
  ssl: true
});

exports.database = client;
client.connect();

/**
* Template fungsi
**/

// export.namafunction = function(){
// 	return sesuatu
// }

exports.selectTableHandler = function(dbResult) {
	if(dbResult.status == 'ok') {
		return {type: '1', rows: dbResult.result.rows};
	} else {
		return {type: '2', rows: 'Koneksi bermasalah'};
	}
};

module.exports.executeQuery = function(query, handlerFunction, additionalArg){
	return new Promise(function(resolve, reject) {
		client.query(query).then(res => {
			resolve(handlerFunction({status: 'ok', result: res, additionalArg: additionalArg}));
		}).catch(e => {
            console.log(e);
			resolve(handlerFunction({status: 'fail', result: e, additionalArg: additionalArg}));
//			client.end();
		});

	});
};

exports.leaveGrup = function(groupId){
	const query = "DELETE FROM agus.participant where group_id = '" + groupId + "';DELETE FROM agus.event WHERE group_id = '" + groupId +"';";
	return module.exports.executeQuery(query, leaveGrupHandler);
};

var leaveGrupHandler = function(dbResult){
	return {type : 'text', text: 'da dah'};
};
