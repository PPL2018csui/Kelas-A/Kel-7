var PLACEHOLDER_IMAGE_URL = 'https://preview.ibb.co/mzajFo/placeholder.png';
var NOTULENSI_WACANA = "https://image.ibb.co/cgwyvy/buat_event.png";
var HELP = "https://image.ibb.co/mmBnoJ/help.png";

var features = [
	{
		thumbnailImageUrl: NOTULENSI_WACANA,
		title: 'Notulen Wacana',
		text: 'Membantu kamu mendata anggota wacana',
		actions: [
			{
				type: 'message',
				label: 'Pilih',
				text: '/create_event'
			}
		]
	},
	{
		thumbnailImageUrl: HELP,
		title: 'Help!!',
		text: 'Klik ini untuk mengenal agus lebih lanjut',
		actions: [
			{
				type: 'message',
				label: 'Pilih',
				text: '/help'
			}
		]
	}
];

exports.join_group = function(){
    var intro = {
		type: 'text',
		text: "Hai! Terima kasih telah mengundang saya. Mulai interaksi dengan memanggil saya. Panggil 'agus'"
	  };

	var tombolIntro = {
		type: "template",
		  altText: "Button tidak tersedia di sini",
		  template:{
			  type: "buttons",
			  actions:[
				  {
					  type: "message",
					  label: "Panggil agus!",
					  text: "agus"
				  }
			  ],
			  title: "Hai! Nama saya Agus",
			  text: "Tekan tombol memanggil saya\n"
		  }
	  };

	return [intro, tombolIntro];
};

exports.introPersonal = function() {
	var intro = {
		type: 'text',
		text: "Hai! Terima kasih telah mengundang saya. Mulai interaksi dengan memanggil saya. Panggil 'agus'"
	  };

	var tombolIntro = {
		type: "template",
		  altText: "Button tidak tersedia di sini",
		  template:{
			  type: "buttons",
			  actions:[
				  {
					  type: "message",
					  label: "Panggil agus!",
					  text: "agus"
				  }
			  ],
			  title: "Hai! Nama saya Agus",
			  text: "Tekan tombol memanggil saya\n"
		  }
	  };

	return [intro, tombolIntro];
};

exports.helpMessage = function(){
	var expectedString = 'Saya bisa apa aja ya?\n'+
						 '\n'+
						 'Untuk event:\n'+
						 '-Ketik /create_event untuk membuat event\n'+
						 '-Ketik /join untuk join event\n'+
						 '-Ketik /cancel untuk membatalkan partisipasi\n'+
						 '-Ketik /remove untuk menghapus event yg sudah ada\n'+
						 '\n'+
						 'Jika Kamu ingin agus keluar dari grup (semoga untuk sesaat saja), ketik /bye';
	return {type: 'text', text: expectedString};
};

exports.helpChooseIt = function(){
	var expectedString = "Ngebuat wacana yang punya opsi\n"+
						 "Yang kaya begini nih:\n\n"+
						 "Minggu Depan Belajar Bareng Kuy:\n"+
						 "-Selasa:\n"+
						 "  1. Adi\n"+
						 "  2. Budi\n"+
						 "-Kamis\n"+
						 "  1. Susi";
	return {type: 'text', text: expectedString};
};

exports.helpListIt = function() {
	var msg = 	"Ngebuat wacana buat ngelist doang\n"+
				"Yang kaya begini nih:\n\n"+
				"Yang ikut Wacana:\n"+
				"1. -\n"+
				"2. -\n"+
				"3. -\n\n"+
				"*Note : Kalo cuma mau ngedata, pilih yang tanpa argumen, kalo mau ada catetan-catetannya, pilih yang dengan argumen"
				;
	return {type: 'text', text:msg};
};

exports.helpListItWithoutArgument = function() {
	var msg = 	"Ngebuat wacana yang ngelist nama akun LINE\n"+
				"Yang kaya begini nih:\n\n"+
				"Yang ikut Wacana:\n"+
				"1. Adi\n"+
				"2. Budi";
	return {type: 'text', text:msg};
};

exports.helpListItWithArgument = function() {
	var msg = 	"Ngebuat wacana yang ngelist nama akun LINE sama catetan-catetannya\n"+
				"Yang kaya begini nih:\n\n"+
				"Cantumin Nomor Hp Dong:\n"+
				"1. Adi - 08xxx\n"+
				"2. Budi - 02xxx";
	return {type: 'text', text:msg};
};

exports.menu = function(params) {
	var carousel = {
		type: 'template',
		altText: 'Pilih perintah "/create_event" untuk membuat wacana atau "/help" untuk informasi lebih lanjut.\n',
		template: {
			type: 'carousel',
			columns: features,
			imageAspectRatio: 'rectangle',
			imageSize: 'cover'
		}
	};
	return carousel;
};
