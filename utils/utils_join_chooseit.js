var funcDB = require("../utils/utils_bot.js");

var checkOptionExist = function(groupId, optionId){
	const query = {
		text: "SELECT * FROM agus.options WHERE group_id=$1 AND id_option=$2",
		values: [groupId, optionId]
	};

	return funcDB.executeQuery(query, checkOptionExistHandler);
};

var joinChooseIt = function (groupId, name, id_option, userid){
	const query = {
		text: "INSERT INTO agus.participant VALUES($1 ,$2, $3, $4, null)",
		values: [groupId, name, id_option, userid],
	};
	return funcDB.executeQuery(query, joinInsertChooseitHandler, {groupId : groupId, id_option: id_option, userid : userid});
};

exports.doChooseIt = function (groupId, userId, profile, inputChooseit){
	return checkOptionExist(groupId, inputChooseit[1]).then(function(optionResult){
		if(optionResult.type == '1'){
			optionName = optionResult.res[0].option_name;
			return joinChooseIt(groupId, profile.displayName, inputChooseit[1] ,userId).then(function(result){
			  if(result.type == '1'){
				return {type: 'text', text: profile.displayName + ' memilih ' + optionName};
			  } else if(result.type == '3'){
				return {type:'text', text: profile.displayName + ' mengganti pilihan menjadi ' + optionName};
			  } else{
				return {type:'text', text: profile.displayName + ', ' +'Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'};
			  }
			});
		}else if (optionResult.type == '2'){
			return {type : 'text', text: profile.displayName + ', Opsi tersebut tidak ada pada pilihan.'};
		}else{
			return {type:'text', text: profile.displayName + ', ' + 'Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'};
		}
	});
};

var checkOptionExistHandler = function(dbResult){
	if(dbResult.status == 'ok' && dbResult.result.rowCount >= 1){
		return {type : '1', res : dbResult.result.rows};
	}else if (dbResult.status == 'ok' && dbResult.result.rowCount == 0){
		return {type : '2'};
	}else{
		return {type: '4'};
	}
};


var joinInsertChooseitHandler = function(dbResult){
	if(dbResult.status == 'ok'){
		return { type: '1'};
	}else{
		if(dbResult.result.code == '23505'){
			//return {type: '3'};
			const query = {
				text: "UPDATE agus.participant SET id_option=$1 WHERE group_id=$2 AND user_id=$3",
				values: [dbResult.additionalArg.id_option, dbResult.additionalArg.groupId, dbResult.additionalArg.userid]
			};
			return funcDB.executeQuery(query, joinUpdateChooseitHandler);
		} else{
			return {type: '4', text:'Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'};
		}
	}
};

var joinUpdateChooseitHandler = function(dbResult){
	if(dbResult.status == 'ok'){
		return {type: '3'};
	}else{
		return {type: '4', text:'Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'};
	}
};
