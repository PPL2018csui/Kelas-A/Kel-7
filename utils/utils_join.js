var funcDB = require("../utils/utils_bot.js");
const utils_join_chooseit = require("../utils/utils_join_chooseit.js");

var joinInsertDB = function(groupId, name, userid, comment){
	const query = {
		text: "INSERT INTO agus.participant VALUES($1, $2, 0, $3, $4)",
		values: [groupId, name, userid, comment]
	};
	return funcDB.executeQuery(query, joinInsertTableHandler);
};

var joinTanpaArgumenInsertDB = function(groupId, name, userid){
	const query = "INSERT INTO agus.participant VALUES('" + groupId + "', '"+ name +"', " + 0 + ", '" + userid + "', " + null +")";

	return funcDB.executeQuery(query, joinInsertTableHandler);
};

exports.joinTanpaArgumen = function(groupId, userId, profile){
	return checkEventExist(groupId).then(function(result){
		if (result.type == '1' && (result.res.rows[0].id_event_types == 1 || result.res.rows[0].id_event_types == 0)){
			return joinTanpaArgumenInsertDB(groupId, profile.displayName, userId).then(function(result){
			  if(result.type == '1'){
				return {type:'text', text: profile.displayName + ' berhasil join'};
			  } else if(result.type == '3'){
				return {type:'text', text: profile.displayName + ' sudah terdaftar sebelumnya'};
			  } else{
				return{type:'text', text: profile.displayName + ', ' +result.text};
			  }
			});
		}else if (result.type == '1' && result.res.rows[0].id_event_types == 2){
			return {type: 'text', text: profile.displayName + " harus memilih opsi karena event adalah Wacana Kolektif. Contoh '/join_1'"};
		} else if (result.type == '2'){
			return {type:'text', text: profile.displayName + ' belum bisa join karena belum ada event'};
		} else {
			return {type:'text', text: profile.displayName + ', ' +result.text};
		}
	});
};

exports.joinArgumenDanChooseIt = function (groupId, userId, profile,input, inputChooseit){
	return checkEventExist(groupId).then(function(result){
	  if (result.type == '1'){
		  if (result.res.rows[0].id_event_types == 1  && input.length == 0 && inputChooseit[0] == '/join' && (/^[1-9]*$/.test(inputChooseit[1]))){
			  return {type: 'text', text: profile.displayName + ', Event yang telah dibuat bukan Wacana Kolektif.'};
		  }else if(result.res.rows[0].id_event_types == 1){
			  //ListIt argumen
			  return joinInsertDB(groupId, profile.displayName, userId, input).then(function(result){
				if(result.type == '1'){
				  return {type:'text', text: profile.displayName + ' berhasil join'};
				} else if(result.type == '3'){
				  return {type:'text', text: profile.displayName + ' sudah terdaftar sebelumnya'};
				} else{
				  return {type:'text', text: profile.displayName + ', ' +result.text};
				}
			  });
		  }else if(result.res.rows[0].id_event_types == 0){
			  //Listit tanpa argumen
			  return joinInsertDB(groupId, profile.displayName, userId).then(function(result){
				if(result.type == '1'){
				  return {type:'text', text: profile.displayName + ' berhasil join tetapi info tambahan tidak didaftarkan karena berjenis Wacana Tunggal Sederhana.'};
				} else if(result.type == '3'){
				  return {type:'text', text: profile.displayName + ' sudah terdaftar sebelumnya'};
				} else{
				  return {type:'text', text: profile.displayName + ', ' +result.text};
				}
			  });
		  }
		  else{
			  //ChooseIt
			  return utils_join_chooseit.doChooseIt(groupId, userId, profile, inputChooseit).then(function(result){
				  return result;
			  });

		  }
	  } else if (result.type == '2'){
		  return {type:'text', text: profile.displayName + ' belum bisa join karena belum ada event'};
	  } else {
		  return {type:'text', text: profile.displayName + ', ' +result.text};
	  }
  });
};

var checkEventExist = function(groupId){
	const query = {
		text: "SELECT * FROM agus.event WHERE group_id=$1 AND is_active=true",
		values: [groupId]
	};

	return funcDB.executeQuery(query, checkEventExistHandler);
};


var checkEventExistHandler = function(dbResult){
	if(dbResult.status == 'ok' && dbResult.result.rowCount == 1){
		return {type : '1', res : dbResult.result};
	}else if (dbResult.status == 'ok' && dbResult.result.rowCount == 0){
		return {type : '2'};
	}else{
		return {type: '4', text: 'Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'};
	}
};

var joinInsertTableHandler = function(dbResult){
	if(dbResult.status == 'ok'){
		return { type: '1', text: 'berhasil join'};
	}else{
		if(dbResult.result.code == '23505'){
			return {type: '3', text:'anda sudah join'};
		} else{
			return {type: '4', text:'Terjadi suatu kesalahan pada server saat ingin join event. Harap coba beberapa saat lagi :)'};
		}
	}
};
