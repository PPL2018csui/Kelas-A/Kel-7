const utils_bot = require("./utils_bot.js");

var PLACEHOLDER_IMAGE_URL = 'https://preview.ibb.co/mzajFo/placeholder.png';
var WACANA_TUNGGAL = "https://image.ibb.co/m6wyt8/wacana_tunggal.png";
var WACANA_KOLEKTIF = "https://image.ibb.co/iJB90o/wacana_kolektif.png";
var ARGUMEN="https://image.ibb.co/jX2Dvy/wacana_chooseit_argumen.png";
var NO_ARGUMEN = "https://image.ibb.co/nFcJTJ/wacana_choose_It_tanpa_argumen.png";

var createPictureObject = function(url) {
	var picture = {
		type: "image",
		originalContentUrl: url,
		previewImageUrl: url
	};
	return picture;
};

exports.createEvent = function(groupId){
	const query = "INSERT INTO agus.event (group_id) values ('" + groupId + "');";
	return utils_bot.executeQuery(query, createEventHandler);
};

var createEventHandler = function(dbResult){

	if(dbResult.status == 'ok'){
		var events = [
			{
				thumbnailImageUrl: WACANA_TUNGGAL,
				title: 'Wacana Tunggal',
				text: 'Membantu kamu mendata anggota wacana',
				actions: [
					{
						type:'message',
						label:'Pilih',
						text:'/create listit'
					 },
					 {
						type:'message',
						label:'Panggil Bantuan',
						text:'/help tunggal'
					 }
				]
			},
			{
				thumbnailImageUrl: WACANA_KOLEKTIF,
				title: 'Wacana Kolektif',
				text: 'Membantu mendata anggota anda untuk wacana kolektif',
				actions: [
					{
						type:'message',
						label:'Pilih',
						text:'/create chooseit'
					},
					{
						type:'message',
						label:'Panggil Bantuan',
						text:'/help kolektif'
					}
				]
			}
		];

		var chooseEventButton = {
		     type:'template',
		     altText:'Guanakan command\n"/create listit" untuk membuat Wacana Tunggal\natau\n"/create chooseit" untuk membuat Wacana Kolektif',
		     template:{
		        type:'carousel',
				columns: events,
				imageAspectRatio: 'rectangle',
				imageSize: 'cover'
		     }
		  };
		return chooseEventButton;
	}else{
		if(dbResult.result.code == '23505'){
			return {type: 'text',text: 'Anda hanya dapat membuat satu event pada sebuah group/multichat.\n' + 'Ketik command "/remove" untuk menghapus event yang sebelumnya anda buat'};
		}
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}

};

exports.chooseEventListIt = function(groupId){
	return checkActivedEvent({groupId : groupId}, chooseEventListItHandler);
};

var chooseEventListItHandler = function(dbResult){
	if(dbResult.status == 'ok'){

		if(parseInt(dbResult.result[0].rows[0].count) <= 0){
			return {type: 'text',text: 'Maaf, Tidak terdapat event pada group ini\n'+
										'Silahkan lakukan perintah "/create_event" untuk membuat event baru'
					};
		}

		if(parseInt(dbResult.result[1].rows[0].count) <= 0){
			var event_types = [
				{
					thumbnailImageUrl: NO_ARGUMEN,
					title: 'Wacana Tunggal Sederhana',
					text: 'Kalau kamu cuma pengen ngedata aja',
					actions: [
						{
							type:'message',
							label:'Pilih',
							text:'/create listit_1'
						},
						{
							type:'message',
							label:'Panggil Bantuan',
							text:'/help listit_1'
						}
					]
				},
				{
					thumbnailImageUrl: ARGUMEN,
					title: 'Wacana Tunggal Kompleks',
					text: 'Kalau kamu cuma pengen ngedata tapi kompleks',
					actions: [
						{
							type:'message',
							label:'Pilih',
							text:'/create listit_2'
						},
						{
							type:'message',
							label:'Panggil Bantuan',
							text:'/help listit_2'
						}
					]
				}
			];

			var chooseEventListItButton = {
				 type:'template',
				 altText:'Pilih jenis Wacana Tunggal yang kamu mau dengan perintah "/create listit_1" untuk Wacana Tunggal Sederhana atau "/create listit_2" untuk Wacana Tunggal Kompleks.\nBisa juga lakukan perintah "/help listit" untuk informasi lebih lanjut.\n',
			     template: {
					type:'carousel',
					columns: event_types,
			        imageAspectRatio: 'rectangle',
					imageSize: 'cover'
			     }
			};

			return chooseEventListItButton;
		}else{
			return {type: 'text',text: 'Anda tidak dapat meng-edit event yang telah aktif'};
		}

	}else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}
};

exports.setUpListItType = function(groupId, type){
	return checkActivedEvent({groupId: groupId, event_type: type}, updateType);
};

var updateType = function(dbResult){
	if(dbResult.status == 'ok'){
		if(parseInt(dbResult.result[0].rows[0].count) <= 0){
			return {type: 'text',text: 'Maaf, Tidak terdapat event pada group ini\n'+
										'Silahkan lakukan perintah "/create_event" untuk membuat event baru'
					};
		}

		if(parseInt(dbResult.result[1].rows[0].count) <= 0){
			const query = "UPDATE agus.event SET id_event_types = " + dbResult.additionalArg.event_type + " WHERE group_id = '" + dbResult.additionalArg.groupId + "';";
			return utils_bot.executeQuery(query, setUpListItTypeHandler, dbResult.additionalArg);
		}else{
			return {type: 'text',text: 'Anda tidak dapat meng-edit event yang telah aktif'};
		}

	}else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}
};

var setUpListItTypeHandler = function(dbResult){

	if(dbResult.status == 'ok'){
		var event = '';

		if(dbResult.additionalArg.event_type == 0){
			event = 'Wacana Tunggal Sederhana';
		}else{
			event = 'Wacana Tunggal Kompleks';
		}

		var message1 = {type: 'text', text: 'Tipe event telah diubah menjadi ' + event};
		var message2 = {type: 'text', text: 'Tentukan nama event anda dengan cara \n' +
									'Ketik command /event_name <spasi> Nama Event Anda\n\n' +
									'Contoh:\n' +
									'/event_name Besok Belajar Bareng Yuk'
						};

		return [message1, message2];
	}else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}

};

exports.setUpListItName = function(groupId, adminId, eventName){
	return checkActivedEvent({groupId : groupId, adminId : adminId, eventName : eventName}, updateEventName);
};

var updateEventName = function(dbResult){
	if(dbResult.status == "ok"){

		if(parseInt(dbResult.result[0].rows[0].count) <= 0){
			return {type: 'text',text: 'Maaf, Tidak terdapat event pada group ini\n'+
										'Silahkan lakukan perintah "/create_event" untuk membuat event baru'
					};
		}

		if(parseInt(dbResult.result[1].rows[0].count) <= 0){
			const query = {
				text: "UPDATE agus.event SET event_name = $1 WHERE group_id = $2;\n",
				values: [dbResult.additionalArg.eventName, dbResult.additionalArg.groupId]
			};
			return utils_bot.executeQuery(query, activateEvent, dbResult.additionalArg);
		}else{
			return {type: 'text',text: 'Anda tidak dapat meng-edit event yang telah aktif'};
		}
	}else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}
};

var activateEvent = function(dbResult){
	if(dbResult.status == 'ok'){

		const query = "UPDATE agus.event SET is_active='true', admin_id='" + dbResult.additionalArg.adminId + "' where group_id = '" + dbResult.additionalArg.groupId + "';\n" +
						"SELECT id_event_types from agus.event WHERE group_id = '" + dbResult.additionalArg.groupId + "';";

		return utils_bot.executeQuery(query, setUpListItNameHandler, dbResult.additionalArg.eventName);
	}else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}
};

var setUpListItNameHandler = function(dbResult){
	if(dbResult.status == 'ok'){
		var eventType = dbResult.result[1].rows[0].id_event_types;
		if(eventType == 0){
			return setUpListItTanpaArgumenNameHandler(dbResult.additionalArg);
		}else if(eventType == 1){
			return setUpListItArgumenNameHandler(dbResult.additionalArg);
		}else{
			// Choose It
			return setUpChooseItHandler(dbResult.additionalArg);
		}

	}else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}

};

var setUpListItTanpaArgumenNameHandler = function(eventName){

	var intro = {type: 'text',text: 'Wacana "' + eventName + '" berhasil dibuat ^_^' };

	var title = '';
	if (eventName.length > 40){
		title = eventName.substring(0,37) + "...";
	}else{
		title = eventName;
	}


	var joinEventListItButton = {
	     type:'template',
	     altText:'Guanakan command "/join" untuk berpartisipasi',
	     template:{
		    type:'buttons',
		    actions:[
		   	{
		        type:'message',
		        label:'Join',
		        text:'/join'
		    },
		    {
		    	type:'message',
		        label:'Help',
		        text:'/help listit_1'
		    }
		    ],
			title:title,
			text:'Tekan tombol "join" untuk berpartisipasi'
		}
	};
		return [createPictureObject(NO_ARGUMEN), intro, joinEventListItButton];
};


var setUpListItArgumenNameHandler = function(eventName){

	var argumented = {type: 'text',text: 'Wacana "' + eventName + '" berhasil dibuat ^_^\n' +
								'Ketik command /join <spasi> argumen anda\n\n' +
								'Contoh:\n' +
								'/join ini argumen saya'
								};
	return [createPictureObject(ARGUMEN), argumented];

};

var setUpChooseItHandler = function(eventName){
	var intro = {type: 'text',text: 'Wacana "' + eventName + '" berhasil dibuat ^_^' };

	var title = '';
	if (eventName.length > 40){
		title = eventName.substring(0,37) + "...";
	}else{
		title = eventName;
	}

	var chooseOpsiButton = {
	     type:'template',
	     altText:'Guanakan command\n"/create chooseit_1" untuk membuat Wacana Kolektif dengan 1 opsi\n"/create chooseit_2" untuk membuat Wacana Kolektif dengan 2 opsi\ndst ...',
	     template:{
	        type:'buttons',
	        actions:[
	           {
	              type:'message',
	              label:'2',
	              text:'/create chooseit_2'
	           },
	           {
	              type:'message',
	              label:'3',
	              text:'/create chooseit_3'
	           },
	           {
	              type:'message',
	              label:'4',
	              text:'/create chooseit_4'
	           }
	        ],
	        title:title,
	        text:'Pilih berapa opsi yang tersedia untuk event ini'
	     }
	  };

	return [intro, chooseOpsiButton];
};

var checkActivedEvent = function(params, handler){
	const query = "SELECT count(*) FROM agus.event WHERE group_id='" + params.groupId + "';\n" +
					"SELECT count(*) FROM agus.event WHERE group_id='" + params.groupId + "' AND is_active='true';";
	return utils_bot.executeQuery(query, handler, params);
};
