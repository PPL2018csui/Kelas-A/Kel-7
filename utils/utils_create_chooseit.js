const utils_bot = require("./utils_bot.js");
const utils_show = require("./utils_show.js");

exports.setUpChooseItType = function(groupId, adminId, type){
	const query = "UPDATE agus.event SET id_event_types = " + type + " WHERE group_id = '" + groupId + "';";
	return utils_bot.executeQuery(query, setUpChooseItTypeHandler);
};

var setUpChooseItTypeHandler = function(dbResult){

	if(dbResult.status == 'ok'){
		return {type: 'text',text: 'Tentukan nama event anda dengan cara \n' +
									'Ketik command /event_name <spasi> Nama Event Anda\n\n' +
									'Contoh:\n' +
									'/event_name Besok Belajar Bareng Yuk'
									};
	}else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}

};

exports.insertOptions = function(groupId, jumlahOpsi, current){
	temp = current + 1;
	const query = "INSERT INTO agus.options (group_id, id_option) values ('" + groupId + "', '"+ temp +"');";
	return utils_bot.executeQuery(query, insertOptionsHandler, {groupId:groupId, jumlahOpsi: jumlahOpsi, current:current});
};

var insertOptionsHandler = function(dbResult){
	if (dbResult.status == 'ok'){
		var curr = parseInt(dbResult.additionalArg.current) + 1;
		if (curr < dbResult.additionalArg.jumlahOpsi){
			return module.exports.insertOptions(dbResult.additionalArg.groupId, dbResult.additionalArg.jumlahOpsi, curr);
		}
		message = {type: 'text', text:'Masukkan opsi sebanyak yang anda pilih dengan perintah /opsi ...\ncontoh "/opsi1 hari minggu"'};
		return message;
	} else{
		if(dbResult.result.code == '23505'){
			return {type: 'text',text: 'Jumlah opsi pada event ini telah dipilih!'};
		}else{
			return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
		}
	}
};
exports.startInsertOption = function(groupId, optionId, optionName){
	const query = "select from agus.options where is_active = 'true' and group_id ='" + groupId + "';";
	return utils_bot.executeQuery(query, startInsertOptionHandler, {groupId: groupId, optionId: optionId, optionName: optionName});
};

var startInsertOptionHandler = function(dbResult){
	if(dbResult.status == 'ok'){
		if(dbResult.result.rowCount != 0){
			return {type: 'text', text: 'Opsi telah dikonfirmasi, tidak bisa diubah lagi'};
		} else{
			return insertOptionName(dbResult.additionalArg.groupId, dbResult.additionalArg.optionId, dbResult.additionalArg.optionName);
		}
	} else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}
};

var insertOptionName = function(groupId, optionId, optionName){
	const query = {
		text: "UPDATE agus.options SET option_name = $3 WHERE group_id = $1 AND id_option = $2",
		values: [groupId, optionId, optionName]
	};
	return utils_bot.executeQuery(query, insertOptionNameHandler, {groupId:groupId, optionId:optionId, optionName:optionName});
};

var insertOptionNameHandler = function(dbResult){
	if(dbResult.status == 'ok'){
		if(dbResult.result.rowCount == 0){
			return {type: 'text',text: 'Masukkan nomor opsi sesuai dengan pilihan jumlah opsi!'};
		}else{
			return checkNamedOptions(dbResult.additionalArg);
		}
	} else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}
};

var checkNamedOptions = function(params){
	const query = "SELECT id_option, option_name from agus.options where group_id ='" + params.groupId + "';";
	return utils_bot.executeQuery(query, checkNamedOptionsHandler, params);
};

var checkNamedOptionsHandler = function(dbResult){
	var msg;
	if(dbResult.status == 'ok'){
		if(dbResult.result.rows.length != 0){
			var temp = '';
			var name = '';
			var optionNullCount = 0;
			for (i = 0; i < dbResult.result.rows.length; i++){
				if (dbResult.result.rows[i].option_name == null){
					name = ' - ';
					optionNullCount++;
				}
				else {
					name = dbResult.result.rows[i].option_name;
				}
				temp = temp + 'Opsi ' + dbResult.result.rows[i].id_option + ' = ' + name + "\n";
			}

			if(optionNullCount == 0){
				msg = {type: 'text', text: 'list opsi\n' + temp};
				var joinEventListItButton = {
			    type:'template',
			    altText:'Guanakan command "/opsi confirm" untuk konfirmasi opsi',
			    template:{
					type:'buttons',
				    actions:[
				   	{
				        type:'message',
				        label:'Confirm',
				        text:'/opsi confirm'
				    },
				    ],
					title: 'Konfirmasi Opsi?',
					text:'Opsi yang telah dikonfirmasi tidak dapat berubah lagi',
				}
				};
				return [msg,joinEventListItButton];
			}


			msg = {type: 'text', text: 'List opsi:\n' + temp};
			return msg;
		} else{
			msg = {type: 'text', text: 'Event telah terhapus'};

			return msg;
		}
	} else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}
};

exports.confirmOptions = function(groupId){
	const query = "UPDATE agus.options SET is_active = 'true' WHERE group_id = '" + groupId + "';";
	return utils_bot.executeQuery(query, confirmOptionsHandler, groupId);
};

var confirmOptionsHandler = function(dbResult){
	if(dbResult.status == 'ok'){
		//implement button for join chooseit here
		return utils_show.getEveryOptionName(dbResult.additionalArg).then(function(res) {
			return utils_show.buildChooseItButton(res.res);
		});
	} else{
		return {type: 'text',text: 'Terjadi kesalahan pada server, silahkan coba beberapa saat lagi'};
	}
};
