class ErrorDB extends Error {
	constructor(message) {
		super(message);
	    this.name = "Error Database";
	}
}

class SelectionError extends ErrorDB{
	constructor(message){
		super(message);
		this.name = "Error Insert"
	}
}

exports.ErrorDB = ErrorDB;
exports.SelectionError = SelectionError;